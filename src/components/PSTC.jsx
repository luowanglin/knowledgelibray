import React,{Component} from 'react';

import { observer, inject } from "mobx-react";

import ProcessModal from "./ProcessModal";

//修改DatePicker语言配置
import locale from 'antd/lib/date-picker/locale/zh_CN';
import { Divider, Row, Col, Table, Input, DatePicker, Button, Popconfirm } from 'antd';
const { TextArea } = Input;


const styles = {
    row:{
        marginTop:20,
        textAlign:'left',
    },
    col:{
        display:'flex'
    },
    span:{
        minWidth:85
    }
};

@inject("store")
@observer
class PSTC extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            modalData: {}
        }

        this.store = this.props.store.pstcStores;
    }

    componentDidMount = () => {
        //清空数据
        this.store.emptyData();
    }

    handleAdd = () => {
        this.setState({
            visible: true,
            modalData: {}
        });
    }

    handleEdit = (e) => {
        this.setState({
            visible: true,
            modalData: e
        });
    }

    handleCancel = () => {
        this.setState({
            visible: false
        });
    }    

    handleDelete = (key) => {
        const store = this.store;
        store.processDelete(key);
    }

    render(){
        const store = this.store;

        const columns = [{
            title: '执行步骤',
            dataIndex: 'opsSchedule',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: 'P-负责人员',
            dataIndex: 'pAdmin',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: 'T-完成期限',
            dataIndex: 'tFinishTime',
            width: "15%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: 'S-责任人完成标准、主管人验收标准',
            dataIndex: 'completeStand',
            width: "20%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '向谁汇报',
            dataIndex: 'render',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '成效验收',
            dataIndex: 'result',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '运行维护流程',
            dataIndex: 'proces',
            width: "15%",
            render: (text, record) => {
                let proces = record.proces.split("/");
                return proces.map((item, index) => {
                    let knowledgeId = item.split(",")[0];
                    let title = item.split(",")[1];
                    return <p key={knowledgeId} title={title} style={{margin: 0}}>{title}</p>
                })
            }
        }, {
            title: '操作',
            key: 'action',
            width: '10%',
            render: (text, record) => (
                <span>
                    {/* <a className="ant-icon add" href="javascript:" onClick={() => this.handleAdd}></a> */}
                    <a className="ant-icon edit" href="javascript:" onClick={() => this.handleEdit(record)}></a>
                    <Popconfirm title="确认要删除吗?" onConfirm={() => this.handleDelete(record.key)}>
                        <a className="ant-icon del_table" href="javascript:;" ></a>
                    </Popconfirm>
                </span>
            ),
        }];
        return(
            <div>
                <Row style={styles.row}>
                    <Col style={styles.col} span={8}>
                        <span className="ant-input-title" title="计划负责人" style={styles.span} >计划总负责人</span>
                        <Input 
                            value={store.planAdmin}
                            placeholder="计划负责人" 
                            style={{width: 300}}
                            onChange={(e) => store.changePlanAdmin(e.target.value)}/>
                    </Col>
                    <Col style={styles.col} span={8}>
                        <span style={styles.span} className="ant-input-title" title="最后完成期限">最后完成期限</span>
                        <DatePicker 
                            locale={locale}
                            showTime
                            format="YYYY-MM-DD HH:mm:ss"
                            value={store.lastTime}
                            style={{ width: 300 }}
                            onChange={store.bindLastTime}/>
                    </Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="验收结案目标" style={styles.span}>验收结案目标</span>
                        <TextArea 
                            className="ant-input-textarea"
                            value={store.resultSum}
                            placeholder="验收结案目标" 
                            autosize={{ minRows: 2, maxRows: 6 }} 
                            onChange={(e) => store.changeResultSum(e.target.value)}/>
                    </Col>
                </Row>
                {/* <Row style={styles.row} >
                    <Col style={styles.col} span={12}>
                        <span style={styles.span}>执行进程</span>
                    </Col>
                    <Col style={styles.col} span={12}>
                        <Button onClick={store.addProcess} onClick={this.handleAdd} style={{ float: "right" }}>添加进程</Button>
                    </Col>
                    
                </Row> */}
                <Row style={{marginTop: "20px"}}>
                    <Col span={8} style={{ textAlign: 'left', paddingTop: "10px" }}>
                        <span style={{ fontWeight: 'bold',  }}>执行进程</span>
                    </Col>
                    <Col span={16}>
                        <Button type="primary" onClick={this.handleAdd} style={{ float: "right" }}>添加进程</Button>
                    </Col>
                </Row>
                <Row style={styles.row} >
                    <Col style={styles.col} span={24}>
                        <div style={{width:'100%'}} >
                            <ProcessModal visible={this.state.visible} handleCancel={this.handleCancel} modalData={this.state.modalData}/>

                            <Table size="small" columns={columns} dataSource={store.processList.slice()} pagination={false}/>
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default PSTC;