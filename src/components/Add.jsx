import React, { Component } from 'react';

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import { observer, inject } from "mobx-react";

import API from "../utils/API.js";

import SubDetail from './SubDetail';

//PDCA组件
import PDCA from './PDCA';

//PSTC组件
import PSTC from "./PSTC";

//运行维护组件
import OPS from "./OPS";

//通用组件
import General from "./General";

//获取时间工具
import { getCurrentDate } from "../utils/getDate";
//时间戳转换
import { getTime } from "../utils/getTime";


import { Layout, Divider, Row, Col, Form, Button, Input, Tag, Icon, Message, Modal, AutoComplete } from 'antd';
// const FormItem = Form.Item;
const { TextArea } = Input;
const InputGroup = Input.Group;
const { Header, Content } = Layout;
const Option = AutoComplete.Option;


@inject("store")
@observer
class Add extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            devicename: "",
            deviceKey: "",
            astCode: "",
            dataSource: []
        }
        this.store = this.props.store.addStores;
    }

    createData = (paramData) => {
        if(!this.store.title) {
            Message.warning("知识标题不能为空！");
            return;
        }else if(!this.store.serialNumber) {
            Message.warning('知识编码不能为空！');
            return;
        }else if(!this.store.typeCode) {
            Message.warning("知识类型不能为空！");
            return;
        }
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: API.BASE_URL + '/api/addKnowledge?token=' + API.token + '',
            data: JSON.stringify(paramData),
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (!response.data.error) {
                    Message.success("数据创建成功！");
                    this.props.history.push('/kms');
                    //刷新数据
                    this.props.store.homeStores.onSearchKnowledge();

                    //清空设备
                    this.store.empryAttachDevices();
                    
                    if(this.store.rootType === "PDCA") {
                        this.props.store.pdcaStores.emptyPstcPlans();
                    }

                    return;
                }
                Message.error(response.data.error);
            },
            error: (error) => {
                Message.error("数据创建失败！");
            }
        });
    }

    handleConfirm = () => {
        const pdcaStores = this.props.store.pdcaStores;
        const pstcStores = this.props.store.pstcStores;
        const generalStores = this.props.store.generalStores
        let paramData = {
            "knowledge": {
                //公共部分
                "title": this.store.title,
                "knowType": this.store.knowType,
                "typeFatherCode": this.store.fatherCode,
                "typeCode": this.store.typeCode,
                "serialNumber": this.store.serialNumber,
                "description": this.store.description,
                "devices": this.store.attachDevices,
                "time": getCurrentDate(),
            },
        };
        if(this.store.rootType === "PDCA") {
            let attachId = "";
            let attachIdData = [];
            pdcaStores.attachId.map((item) => {
                attachIdData.push(item);
            });
            pdcaStores.pstcPlans.map((item) => {
                attachIdData.push(item.id);
            })
            attachIdData.map((item, index) => {
                if (index >= attachIdData.length - 1) {
                    attachId += "" + item + "";
                } else {
                    attachId += "" + item + ",";
                }
            });
            paramData.knowledge["plan"] = pdcaStores.plan;
            paramData.knowledge["doSomething"] = pdcaStores.doSomething;
            paramData.knowledge["checkout"] = pdcaStores.checkout;
            paramData.knowledge["improvementAction"] = pdcaStores.impromentAction;
            paramData.knowledge["attachId"] = attachId;
        } else if (this.store.rootType === "PSTC") {
            paramData.knowledge["planAdmin"] = pstcStores.planAdmin;
            paramData.knowledge["lastTime"] = getTime(pstcStores.lastTime);
            paramData.knowledge["resultSum"] = pstcStores.resultSum;            
            paramData["plans"] = pstcStores.processList;
        }else{
            paramData["processs"] = generalStores.process;
        }

        this.createData(paramData);
    }

    handleCancel = () => {
        this.props.history.push('/kms');
    }

    //设备搜索
    onSearch = (value) => {
        this.setState({
            devicename: "",
            deviceKey: value,
            astCode: ""
        });
        $.ajax({
            url: API.BASE_URL + '/api/findDeviceByKeyword?token=' + API.token + '',
            type: 'GET',
            data: {
                keyword: value
            },
            success: (response) => {
                if (response.data && response.data.status === 200) {
                    const dataSource = [];
                    response.data.resultList.map((item) => {
                        dataSource.push(<Option key={item.ast_code} value={item.ast_code}>{item.name}</Option>);
                    })
                    this.setState({
                        dataSource: dataSource
                    });
                    return;
                }
            },
            error: (error) => {
                Message.error("数据获取失败！");
            }
        });
    }

    onSelectDev = (value, options) => {
        this.setState({
            devicename: options.props.children,
            deviceKey: value,
            astCode: value
        });
    }

    handleAttachDev = () => {
        this.setState({
            visible: true
        });
    }

    //取消绑定设备
    onCancelAttachDevices = () => {
        const store = this.store;
        store.onCancelAttachDevices();
        this.setState({ visible: false });
    }

    //确定绑定设备
    onOkattachDevices = () => {
        const store = this.store;
        store.onOkattachDevices();
        this.setState({visible: false});
    }

    //添加设备
    addDevice = () => {
        if(this.state.astCode) {
            const store = this.store;
            for(let i = 0; i < store.devices.length; i++) {
                if(store.devices[i].astCode === this.state.astCode) {
                    Message.warning("您已经关联此设备！");
                    return false;
                }
            }
            store.addDevice(this.state.astCode, this.state.devicename);
            this.setState({
                devicename: "",
                deviceKey: "",
                ast_code: ""
            });
            return;
        }
        Message.warning("请选择关联设备！");
    }

    //删除设备
    deleteDevice = (event) => {
        const store = this.store;
        store.deleteDevice(event.currentTarget.getAttribute("data-astcode"));
    }

    render() {
        const store = this.store;
        return (
            <Layout className="home-container ">
                <Header className="header-title" style={{ background: '#fff', padding: "0 20px" }}>
                    <Row type="flex" style={{ textAlign: 'left' }}>
                        <Col span={8} >
                            <Divider type="vertical" style={{ width: 2, backgroundColor: '#00c1d1', height: '20px', marginBottom: '4px', marginRight: "12px", marginLeft: 0 }} />

                            <span style={{ type: 'flex', fontSize: '16px', fontWeight: 'bold', color: "#333", align: 'middle' }}>新增知识</span>
                        </Col>
                        <Col span={16} style={{ textAlign: 'right' }}>
                            <Button type="primary" style={{ marginRight: '18px' }} onClick={this.handleAttachDev}>关联设备</Button>
                            <Button type="primary" style={{ marginRight: '18px' }} onClick={this.handleConfirm}>确定</Button>
                            <Button className="common-btn" onClick={this.handleCancel}>取消</Button>
                        </Col>

                        <Modal
                            title="关联设备"
                            maskClosable={false}
                            visible={this.state.visible}
                            onCancel={this.onCancelAttachDevices}
                            footer={<div><button type="button" className="ant-btn ant-btn-primary" onClick={this.onOkattachDevices}><span>确 定</span></button><button type="button" className="ant-btn" onClick={this.onCancelAttachDevices}><span>取 消</span></button></div>}>
                            <InputGroup compact>
                                {/* <Input 
                                    value={this.state.devicename} 
                                    style={{ width: '80%' }} 
                                    placeholder="请输入关联设备" 
                                    onChange={(e) => this.setState({devicename: e.target.value})}/> */}
                                <AutoComplete 
                                    value={this.state.deviceKey}
                                    onSearch={this.onSearch}
                                    onSelect={this.onSelectDev}
                                    placeholder="请输入关联设备"
                                    dataSource={this.state.dataSource}
                                    style={{ width: '80%' }} />
                                <Button type="primary" style={{width: "20%"}} onClick={this.addDevice}>添加</Button>
                            </InputGroup>
                            <ul className="device-list">
                                {
                                    store.devices.map((item, index) => 
                                        <li key={index} className="device-item">
                                            <span>{item.deviceName}</span>
                                            <a href="javascript:;" className="cir_del" data-astcode={item.astCode} onClick={this.deleteDevice}></a>
                                        </li>
                                    )
                                }
                            </ul>
                        </Modal>

                    </Row>
                    <Divider style={{ width: 'auto', marginTop: 0 }} />
                </Header>
                <Content style={{background: '#fff',height:'100%',top: '0px',bottom: '0px', padding: '0 18px 18px', overflow: 'auto'}}>
                    <SubDetail/>
                    {
                        (() => {
                            switch (store.rootType) {
                                case "PDCA":
                                    return <PDCA />;

                                case "PSTC":
                                    return <PSTC />;

                                default:
                                    return <General />;

                            }
                        })()
                    }
                </Content>
            </Layout>
        );
    }
}

export default Add;
  