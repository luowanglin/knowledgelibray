import React, { Component } from "react";

import { observer, inject } from "mobx-react";

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import API from "../utils/API.js";

//公用删除提示Modal
import DeleteModal from "./common/DeleteModal";

import { Spin, Layout, Tree, Divider, Row, Col, Button, Modal, Input, Message } from "antd";
const TreeNode = Tree.TreeNode;
const { Header, Sider, Content } = Layout;


/**
 *  左边菜单树组件
 */
@inject("store")
@observer
class LeftTree extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            delVisible: false,
            knowledgeType: "",          //知识类型
            childrenCode: [],
            typeCode: "",               //知识类型Code
            fatherCode: "",             //知识类型父节点Code
            rootType: ""                //知识树节点归属类型
        }
        this.store = this.props.store.homeStores;
    }

    onSelect = (key, node) => {
        const store = this.store;
        // const children = node.node.props.children;
        // const childrenCode = [];
        // children && children.map((item) => {
        //     childrenCode.push(item.key);
        // });
        //过滤报表数据
        store.onSelect(key, node);
        // this.setState({
        //     childrenCode: childrenCode
        // });
    }

    onCheck = (checkedKeys, info) => {
        const store = this.store;
        const children = info.node.props.children;
        const childrenCode = [];
        children && children.map((item) => {
            childrenCode.push(item.key);
        });
        this.setState({
            childrenCode: childrenCode
        });
        
        store.onCheck(checkedKeys, info);
    }

    onExpand = (expandedKeys) => {
        this.store.onExpand(expandedKeys);
    }

    showAddModal = () => {
        this.setState({
            visible: true,
            knowledgeType: "",
            typeCode: ""
        });
    }

    showEditModal = (event) => {
        if(event.node.props.modifi) {
            const typeCode = event.node.props.eventKey
            const knowledgeType = event.node.props.value;
            const fatherCode = event.node.props.fatherCode;
            const rootType = event.node.props.rootType;
            this.setState({
                visible: true,
                knowledgeType: knowledgeType,
                typeCode: typeCode,
                fatherCode: fatherCode,
                rootType: rootType
            });
            return;
        }
        Message.warning("系统内置数据不可编辑！");
    }

    onCancel = () => {
        this.setState({
            visible: false
        });
    }

    showDeleteModal = () => {
        if (this.store.checkedKeys.length) {
            // if (!this.store.modifi) {
            //     Message.warning("系统内置数据不可删除！");
            //     return;
            // }
            this.setState({ delVisible: true });
            return;
        }
        Message.warning("未选中知识类型！");
    }

    onOk = () => {
        if(!this.state.typeCode){
            this.addKnowledgeType();
        }else{
            this.updateKnowledgeType();
        }
        
    }

    //添加知识树类型
    addKnowledgeType = () => {
        if(!this.state.knowledgeType) {
            Message.warning("知识树内容不能为空！");
            return;
        }
        const store = this.store;
        $.ajax({
            url: API.BASE_URL + '/api/addKnowledgeType?token='+API.token+'',
            type: 'POST',
            contentType: 'application/json',
            // xhrFields: {
            //     withCredentials: true
            // },
            data: JSON.stringify({
                // userId: store.userId,
                rootType: this.store.rootType || this.state.knowledgeType,
                knowledgeType: this.state.knowledgeType,
                fatherCode: this.store.fatherCode,
                modifi: true,
            }),
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (!response.data.error) {
                    this.setState({ visible: false });
                    //刷新数据
                    store.loadTreeData();
                    Message.success("添加数据成功！");
                    return;
                }
                Message.error("数据添加失败！");
            },
            error: () => {
                Message.error("数据添加失败！");
            }
        });
    }

    //修改知识树类型
    updateKnowledgeType = () => {
        if (!this.state.knowledgeType) {
            Message.warning("知识树内容不能为空！");
            return;
        }
        const store = this.store;
        $.ajax({
            url: API.BASE_URL + '/api/updateKnowledgeType?token='+API.token+'',
            type: 'PUT',
            contentType: 'application/json',
            // xhrFields: {
            //     withCredentials: true
            // },
            data: JSON.stringify({
                // userId: store.userId,
                knowledgeType: this.state.knowledgeType,
                // fatherCode: this.state.fatherCode,
                typeCode: this.state.typeCode,
                modifi: true,
            }),
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (!response.data.error) {
                    this.setState({ visible: false });
                    //刷新数据
                    store.loadTreeData();
                    Message.success("修改数据成功！");
                    return;
                }
                Message.error("修改数据失败！");
            },
            error: () => {
                Message.error("修改数据失败！");
            }
        });
    }

    //删除知识树类型
    deleteKnowledgeType = () => {
        const store = this.store;
        store.deleteKnowledgeType(this.state.childrenCode, () => {
            this.setState({delVisible: false});
        });
    }

    onDelete = () => {
        this.deleteKnowledgeType();
    }

    render() {
        const store = this.store;
        const modalTitle = this.state.typeCode ? "编辑知识类型" : "添加知识类型";

        const loop = data => data.map((item, index) => {
            if(item.children) {
                return(
                    <TreeNode key={item.typeCode} rootType={item.rootType} fatherCode={item.fatherCode} title={item.knowledgeType} value={item.knowledgeType} modifi={item.modifi} disableCheckbox ={item.modifi ? false : true}>
                        {loop(item.children)}
                    </TreeNode>
                )
            }
            return <TreeNode key={item.typeCode} rootType={item.rootType} fatherCode={item.fatherCode} value={item.knowledgeType} title={item.knowledgeType} modifi={item.modifi} disableCheckbox={item.modifi ? false : true}></TreeNode>
        })
        return (
            <Sider className="tree-box" width={300} style={{ position: 'relative',  background: '#f8f8f8', marginLeft: '20px', marginRight: '20px', marginBottom: '20px' }}>
                <Header style={{ background: '#f8f8f8', height: '40px', padding: 0 }}>
                    <Row style={{ lineHeight: '50px' }}>
                        <Col span={8} style={{ textAlign: 'left', paddingLeft: '20px', fontWeight: 'bold' }}>知识树</Col>
                        <Col span={16} style={{ textAlign: 'right', paddingRight: '20px' }}>
                            <a className="ant-icon add" href="javascript:;" onClick={this.showAddModal}></a>
                            <a className="ant-icon del" href="javascript:;" onClick={this.showDeleteModal}></a>
                        </Col>
                    </Row>
                </Header>
                <Divider style={{ width: 'auto', margin: "0 20px" }} />
                <Content style={{ textAlign: 'left', margin: '0 20px 0 10px' }}>
                    <Tree 
                        checkable
                        checkStrictly={true}
                        expandedKeys={store.expandedKeys.slice()}
                        onExpand={this.onExpand}
                        selectedKeys={store.selectKeys.slice()}
                        checkedKeys={store.checkedKeys.slice()}
                        onRightClick={this.showEditModal}
                        onSelect={this.onSelect}
                        onCheck={this.onCheck}>

                        {loop(this.store.treeData)}
                    </Tree>
                    
                    <Modal
                        centered
                        title={modalTitle}
                        maskClosable={false}
                        visible={this.state.visible}
                        cancelText="取消"
                        okText="确定"
                        onCancel={this.onCancel}
                        footer={<div><button type="button" className="ant-btn ant-btn-primary" onClick={this.onOk}><span>确 定</span></button><button type="button" className="ant-btn" onClick={this.onCancel}><span>取 消</span></button></div>}>
                        
                        <Input placeholder="请输入知识类型" value={this.state.knowledgeType} onChange={(e) => this.setState({knowledgeType: e.target.value})}/>
                    </Modal>

                    <DeleteModal 
                        visible={this.state.delVisible} 
                        onOk={this.onDelete} 
                        onCancel={() => this.setState({delVisible: false})}/>

                </Content>
            </Sider>
        )
    }
}

export default LeftTree;