import React, { Component } from 'react';

// import { Link } from "react-router-dom";

import { observer, inject, PropTypes as ObservablePropTypes } from "mobx-react";

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import API from '../utils/API';

import '../styles/Home.css';

import LeftTree from "./LeftTree";

//本地ICON
import see from "../images/see.png";
import edit from "../images/edit.png";
import del_table from "../images/del_table.png";

import { Spin, Layout, Divider, Row, Col, Button, Input, Table, Popconfirm, Message, Modal } from 'antd';
const { Header, Sider, Content } = Layout;
const Search = Input.Search;


@inject("store")
@observer
class Home extends Component {

    constructor(props){
        super(props);
        this.state = { 
            
        }; 
        this.store = this.props.store.homeStores;
    }

    componentDidMount() {
        // const store = this.store;
        // store.getUserId(function (userId) {
        //     store.loadKnowledge(userId);
        //     store.loadTreeData(userId);
        // });

    }   

    previewAction = (text, record)=>{
        // this.props.history.push('/preview?knowledgeId=' + record.knowledgeId + '');
        const previewStores = this.props.store.previewStores;
        this.store.onChangeKnowledgeId(record);
        
        //查询知识归属类型
        this.store.findKnowledgeType(record.typeCode, (data) => {
            //设置知识归属类型
            previewStores.setRootType(data);
            
            //获取知识详细信息
            previewStores.getKnowledgeContent(record.knowledgeId, () => {
                //数据加载成功的时候
                this.props.history.push('/kms/preview');
            }, () => {
                //数据被删除的时候
                this.props.history.push('/kms');
                this.store.onSearchKnowledge();
            });
        });
        
    }

    editAction = (text, record)=>{
        // this.props.history.push('/edit?knowledgeId=' + record.knowledgeId + '');
        const editStores = this.props.store.editStores;
        //获取知识ID
        this.store.onChangeKnowledgeId(record);

        //查询知识归属类型
        this.store.findKnowledgeType(record.typeCode, (data) => {
            //清空PDCA添加的pstc列表
            if(data === 'PDCA') {
                editStores.emptyPstcPlans();
            }
            //设置知识归属类型
            editStores.setRootType(data);

            //获取知识详细信息
            editStores.getKnowledgeContent(record.knowledgeId, () => {
                //数据加载成功的时候
                this.props.history.push('/kms/edit');
            }, () => {
                //数据被删除的时候
                this.props.history.push('/kms');
                this.store.onSearchKnowledge();
            });
        });
    }    

    addAction = () => {
        this.props.history.push('/kms/add');
    }

    onSearch = (value, event) => {
        const store = this.store;
        store.onSearch(value);
    }   

    onDelete = (key) => {
        this.store.deleteKnowledge(key);
        this.setState({visible: false});
    }

    download = (key) => {
        // $.ajax({
        //     url: API.BASE_URL + "/api/genWord?token="+API.token+"",
        //     data: {
        //         knowledgeId: key
        //     },
        //     success: (response) => {
        //         if(response.code === 404){
        //             Message.error(response.msg);
        //             this.store.onSearchKnowledge();
        //             return;
        //         }

        //         //下载知识
                // const downloadUrl = API.BASE_URL + "/api/genWord?" + $.param({
                //     knowledgeId: key,
                //     token: API.token
                // });
        //         window.open(downloadUrl);
        //     },
        //     error: () => {

        //     }
        // });
        // const downloadUrl = API.BASE_URL + "/api/genWord?token="+API.token;
        // let formData = new FormData();
        // formData.append("knowledgeId",key);
        // var opts = {
        //     method:"POST",
        //     body:formData
        // }
        // window.location.href = downloadUrl;
        // window.event.returnValue = true;
        const downloadUrl = API.BASE_URL + "/api/genWord?" + $.param({
            knowledgeId: key,
            token: API.token
        });
        var fileName;
        fetch(downloadUrl)
            .then(response => {
                fileName = response.headers.get("Content-Disposition").split("=").pop();
                fileName = decodeURIComponent(fileName);
                return response.blob()
            })
            .then(blob => {
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = fileName;
                link.click();
                window.URL.revokeObjectURL(link.href);
            });
    }
    
    render() {
        const store = this.store;

        const columns = [{
            title: '标题',
            dataIndex: 'title',
            width: "15%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '编号',
            dataIndex: 'serialNumber',
            width: "15%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '描述',
            dataIndex: 'description',
            width: "20%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '创建人',
            dataIndex: 'creatName',
            width: "15%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '创建时间',
            dataIndex: 'createTime',
            width: "20%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '操作',
            key: 'action',
            render: (text, record) => (
                <span>
                    <a className="ant-icon see" href="javascript:;" onClick={() => this.previewAction(text, record)}></a>
                    <a className="ant-icon edit" href="javascript:" onClick={() => this.editAction(text, record)}></a>
                    {/* <Link to={{ pathname: "preview"}}><a className="ant-icon see" href="javascript:" ></a></Link>
                    <Link to={{ pathname: "edit" }}><a className="ant-icon edit" href="javascript:" ></a></Link> */}
                    <a className="ant-icon download" href="javascript:" onClick={() => this.download(record.knowledgeId)}></a>
                    <Popconfirm title="确认要删除吗?" onConfirm={() => this.onDelete(record.key)}>
                        <a className="ant-icon del_table" href="javascript:;" ></a>
                    </Popconfirm>
                </span>
            ),
            width: "15%",
        }];

        return (
            // <Spin spinning={store.loading}>
                <Layout className="home-container">
                    <Header className="header-title" style={{ background: '#fff', padding: "0 20px"}}>
                        <Row type="flex" style={{ textAlign: 'left' }}>
                            <Col span={8} >
                                <Divider type="vertical" style={{ width: 2, backgroundColor: '#00c1d1', height: '20px', marginBottom: '4px', marginRight: "12px", marginLeft:0 }} />

                                <span style={{ type: 'flex', fontSize: '16px', fontWeight: 'bold', color: "#333", align: 'middle' }}>知识库</span>
                            </Col>
                            <Col span={16} style={{ textAlign: 'right'}}>
                                {/* <Button type="primary" style={{ marginRight: '18px' }}>创建工单</Button> */}
                                {/* <Button type="primary" onClick={this.download} style={{ marginRight: '18px' }}>下载</Button> */}
                                <Button type="primary" onClick={this.addAction}>新增知识</Button>
                            </Col>
                        </Row>
                        <Divider style={{ width: 'auto', marginTop: 0 }} />
                    </Header>
                    <Layout style={{ background: '#fff', height: '100%', top: '0px', bottom: '0px', paddingTop: '20px' }}>
                        {/* <Divider style={{ width: 'auto', marginTop: 0, marginLeft: '20px', marginRight: '20px' }} /> */}
                        <LeftTree />

                        {/* <DeleteModal visible={this.state.visible} onOk={this.onDelete} onCancel={this.onCancel}/> */}

                        <Content style={{ marginRight: '20px' }}>
                            <Row style={{ marginBottom: '15px' }}>
                                <Col span={8}>
                                    <Search
                                        defaultValue={store.keyword}
                                        style={{background: '#fff'}}
                                        placeholder="请输入关键字"
                                        enterButton="搜索"
                                        onSearch={this.onSearch} />
                                </Col>
                            </Row>
                            <Row>
                                <Col span={24}>
                                    <Table 
                                        pagination={store.pagination}
                                        size="small"
                                        columns={columns} 
                                        dataSource={store.dataSource.slice()} />
                                </Col>
                            </Row>
                        </Content>
                    </Layout>
                </Layout>
            // </Spin>
        );
    }
  }
  
  export default Home;
  