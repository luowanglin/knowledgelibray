import { observable, action } from "mobx";

import { Message } from "antd";

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import API from "../utils/API.js";


class PreviewStores {

    @observable loading;            //加载状态
    @observable rootType;           //知识归属类型
    @observable previewDetailData;  //查看页面详细信息数据

    //通用模块
    @observable processId;          //流程分类ID
    @observable proType;            //流程分类类型


    constructor() {
        this.loading = false;
        this.rootType = "";
        this.previewDetailData = null;
        this.processId = [];
        this.proType = "";
    }
   
    //获取详细知识条目
    @action getKnowledgeContent = (knowledgeId, successCallback, errorCallback) => {
        this.loading = true;
        $.ajax({
            type: 'GET',
            url: API.BASE_URL + '/api/getKnowledgeContent?token='+API.token+'',
            // xhrFields: {
            //     withCredentials: true
            // },
            data: {
                knowledgeId: knowledgeId
            },
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if ( response.data.data === null) {
                    Message.error("内容已被移除！");
                    errorCallback && errorCallback();
                    return;
                }
                this.previewDetailData = response.data;
                this.processId = response.data.processContent.length ? [JSON.stringify(response.data.processContent[0].processId)] : [];
                this.proType = response.data.processContent.length ? response.data.processContent[0].proType : "";
                
                //数据加载成功的回调
                successCallback && successCallback();
            },
            error: () => {
                Message.error("获取数据失败！");
            }
        }).always(() => {
            this.loading = false;
        });
    }

    //流程分类onSelect
    onSelectType = (e) => {
        this.processId = [e.key];
        this.proType = e.item.props.children;
    }

    @action setRootType = (data) => {
        this.rootType = data;
    }

    //清空数据
    @action emptyData = () => {
        // this.rootType = "";
        this.previewDetailData = null;
        this.processId = [];
        this.proType = "";
    }

}

const previewStores = new PreviewStores();
export default previewStores;