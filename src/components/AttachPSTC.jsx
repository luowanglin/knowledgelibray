import React, { Component } from 'react';

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import { observer, inject } from "mobx-react";

import API from "../utils/API.js";

import SubDetail from './SubDetail';

//PSTC组件
import PSTC from "./PSTC";

//获取时间工具
import { getCurrentDate } from "../utils/getDate";
//时间戳转换
import { getTime } from "../utils/getTime";


import { Layout, Divider, Row, Col, Form, Button, Input, Tag, Icon, Message, Modal, AutoComplete, Cascader } from 'antd';
const { TextArea } = Input;
const InputGroup = Input.Group;
const { Header, Content } = Layout;
const Option = AutoComplete.Option;

const styles = {
    row: {
        marginTop: 20,
        textAlign: 'left',
    },
    col: {
        display: 'flex'
    },
    span: {
        minWidth: 85
    }
};


@inject("store")
@observer
class Add extends Component {

    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            devicename: "",
            deviceKey: "",
            dataSource: []
        }
        this.store = this.props.store.attachpstcStores;
    }

    componentDidMount = () => {
        //清空数据
        this.store.emptyData();

        //页面刷新之后跳回主页
        const addStores = this.props.store.addStores;
        if(addStores.typeCode.length) {
            return;
        }
        this.props.history.push("/kms");
    }

    createData = (paramData) => {
        if (!this.store.typeCode.length) {
            Message.warning("知识类型不能为空！");
            return;
        }
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: API.BASE_URL + '/api/addKnowledge?token=' + API.token + '',
            data: JSON.stringify(paramData),
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (!response.data.error) {
                    this.props.store.pdcaStores.addPstcPlans(response.data);
                    Message.success("数据创建成功！");
                    this.props.history.goBack();
                    //刷新数据
                    // this.props.store.homeStores.onSearchKnowledge();
                    return;
                }
                Message.error(response.data.error);
            },
            error: (error) => {
                Message.error("数据创建失败！");
            }
        });
    }

    handleConfirm = () => {
        const pstcStores = this.props.store.pstcStores;
        let paramData = {
            "knowledge": {
                //公共部分
                "title": this.store.title,
                "knowType": this.store.knowType,
                "typeFatherCode": this.store.typeCode[0],
                "typeCode": this.store.typeCode[this.store.typeCode.length - 1],
                "serialNumber": this.store.serialNumber,
                "description": this.store.description,
                "planAdmin": pstcStores.planAdmin,
                "lastTime": getTime(pstcStores.lastTime),
                "resultSum": pstcStores.resultSum,
                "devices": this.store.attachDevices,
                "time": getCurrentDate(),
            },
            "plans": pstcStores.processList
        };

        this.createData(paramData);
    }

    handleCancel = () => {
        this.props.history.goBack();
    }

    //设备搜索
    onSearch = (value) => {
        this.setState({
            devicename: value,
            deviceKey: ""
        });
        $.ajax({
            url: API.BASE_URL + '/api/findDeviceByKeyword?token=' + API.token + '',
            type: 'GET',
            data: {
                keyword: value
            },
            success: (response) => {
                if (response.data && response.data.status === 200) {
                    const dataSource = [];
                    response.data.resultList.map((item) => {
                        dataSource.push(item.name);
                    })
                    this.setState({
                        dataSource: dataSource
                    });
                    return;
                }
            },
            error: (error) => {
                Message.error("数据获取失败！");
            }
        });
    }

    onSelectDev = (value, options) => {
        this.setState({
            devicename: value,
            deviceKey: options.key
        });
    }

    handleAttachDev = () => {
        this.setState({
            visible: true
        });
    }

    //取消绑定设备
    onCancelAttachDevices = () => {
        const store = this.store;
        store.onCancelAttachDevices();
        this.setState({ visible: false });
    }

    //确定绑定设备
    onOkattachDevices = () => {
        const store = this.store;
        store.onOkattachDevices();
        this.setState({ visible: false });
    }

    //添加设备
    addDevice = () => {
        if (this.state.deviceKey) {
            const store = this.store;
            for (let i = 0; i < store.devices.length; i++) {
                if (store.devices[i].deviceName === this.state.deviceKey) {
                    Message.warning("您已经关联此设备！");
                    return false;
                }
            }
            store.addDevice(this.state.devicename);
            this.setState({
                devicename: "",
                deviceKey: ""
            });
            return;
        }
        Message.warning("请选择关联设备！");
    }

    //删除设备
    deleteDevice = (event) => {
        const store = this.store;
        store.deleteDevice(event.currentTarget.getAttribute("data-devicename"));
    }

    onChange = (value, node) => {
        this.store.onSelectType(value, node);
    }

    render() {
        const store = this.store;
        const treeData = this.props.store.homeStores.treeData;
        const typeCode = store.typeCode.slice();

        const options = [];
        const loop = data => data.map((item, index) => {
            if(item.rootType === "PSTC") {
                options.push({
                    value: String(item.typeCode),
                    label: item.knowledgeType,
                    rootType: item.rootType,
                    children: []
                });
                if (item.children) {
                    item.children.map((childItem) => {
                        options[0].children.push({
                            value: String(childItem.typeCode),
                            label: childItem.knowledgeType,
                            rootType: item.rootType
                        });
                    })
                }
            }
        })
        loop(treeData);

        return (
            <Layout className="home-container ">
                <Header className="header-title" style={{ background: '#fff', padding: "0 20px" }}>
                    <Row type="flex" style={{ textAlign: 'left' }}>
                        <Col span={8} >
                            <Divider type="vertical" style={{ width: 2, backgroundColor: '#00c1d1', height: '20px', marginBottom: '4px', marginRight: "12px", marginLeft: 0 }} />

                            <span style={{ type: 'flex', fontSize: '16px', fontWeight: 'bold', color: "#333", align: 'middle' }}>添加(绑定)PSTC</span>
                        </Col>
                        <Col span={16} style={{ textAlign: 'right' }}>
                            <Button type="primary" style={{ marginRight: '18px' }} onClick={this.handleAttachDev}>关联设备</Button>
                            <Button type="primary" style={{ marginRight: '18px' }} onClick={this.handleConfirm}>确定</Button>
                            <Button className="common-btn" onClick={this.handleCancel}>取消</Button>
                        </Col>

                        <Modal
                            title="关联设备"
                            maskClosable={false}
                            visible={this.state.visible}
                            onCancel={this.onCancelAttachDevices}
                            footer={<div><button type="button" className="ant-btn ant-btn-primary" onClick={this.onOkattachDevices}><span>确 定</span></button><button type="button" className="ant-btn" onClick={this.onCancelAttachDevices}><span>取 消</span></button></div>}>
                            <InputGroup compact>
                                <AutoComplete
                                    value={this.state.devicename}
                                    onSearch={this.onSearch}
                                    onSelect={this.onSelectDev}
                                    placeholder="请输入关联设备"
                                    dataSource={this.state.dataSource}
                                    style={{ width: '80%' }} />
                                <Button type="primary" style={{ width: "20%" }} onClick={this.addDevice}>添加</Button>
                            </InputGroup>
                            <ul className="device-list">
                                {
                                    store.devices.map((item, index) =>
                                        <li key={index} className="device-item">
                                            <span>{item.deviceName}</span>
                                            <a href="javascript:;" className="cir_del" data-devicename={item.deviceName} onClick={this.deleteDevice}></a>
                                        </li>
                                    )
                                }
                            </ul>
                        </Modal>

                    </Row>
                    <Divider style={{ width: 'auto', marginTop: 0 }} />
                </Header>
                <Content style={{ background: '#fff', height: '100%', top: '0px', bottom: '0px', paddingLeft: '18px', paddingRight: '18px' }}>
                    <Row style={styles.row}>
                        <Col style={styles.col} span={8}>
                            <span className="ant-input-title" title="知识标题" style={styles.span} >知识标题</span>
                            <Input
                                value={store.title}
                                placeholder="知识标题"
                                style={{ width: 300 }}
                                onChange={(e) => store.changeTitle(e.target.value)} />
                        </Col>
                        <Col style={styles.col} span={8}>
                            <span className="ant-input-title" title="知识编码" style={styles.span} >知识编码</span>
                            <Input
                                value={store.serialNumber}
                                placeholder="知识编码"
                                style={{ width: 300 }}
                                onChange={(e) => store.changeSerialNumber(e.target.value)} />
                        </Col>
                        <Col span={8} style={{ visibility: this.props.visibilityTypeSelect, display: 'flex' }}>
                            <span className="ant-input-title" title="知识类型" style={styles.span} >知识类型</span>
                            <Cascader
                                value={typeCode}
                                placeholder="请选择知识类型"
                                options={options}
                                onChange={this.onChange} />
                        </Col>
                    </Row>
                    <Row style={{ marginTop: '20px', textAlign: 'left' }} >
                        <Col span={24} style={{ display: 'flex' }} >
                            <span className="ant-input-title" title="知识描述" style={styles.span} >知识描述</span>
                            <TextArea
                                className="ant-input-textarea"
                                autosize={{ minRows: 2, maxRows: 6 }}
                                value={store.description}
                                placeholder="知识描述"
                                onChange={(e) => { store.changeDescription(e.target.value) }} />
                        </Col>
                    </Row>

                    <PSTC />
                </Content>
            </Layout>
        );
    }
}

export default Add;
