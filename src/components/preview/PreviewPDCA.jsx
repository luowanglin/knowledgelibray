import React, { Component } from 'react';

import { observer, inject } from "mobx-react";

//通用类型组件
import PreviewGeneral from "./PreviewGeneral.jsx";

//PSTC类型组件
import PreviewPSTC from "./PreviewPSTC.jsx";

import { Layout, Row, Col, Input } from 'antd';
const { Header, Content } = Layout;
const { TextArea } = Input;

const styles = {
    row: {
        marginTop: 20,
        textAlign: 'left',
    },
    col: {
        display: 'flex'
    },
    span: {
        minWidth: 85
    }
};

@inject("store")
@observer
class PDCA extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        const { knowledge, attachKnowledge } = this.props;

        return (
            <div>
                <Row style={styles.row}>
                    <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="计划(Plan)" style={styles.span} >计划(Paln)</span>
                        <TextArea
                            className="ant-input-textarea"
                            autosize={{ minRows: 2, maxRows: 6 }} 
                            disabled={true}
                            value={knowledge.plan} />
                    </Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="执行(Do)" style={styles.span}>执行(Do)</span>
                        <TextArea
                            className="ant-input-textarea"
                            autosize={{ minRows: 2, maxRows: 6 }} 
                            disabled={true}
                            value={knowledge.doSomething} />
                    </Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="检查(Check)" style={styles.span}>检查(Check)</span>
                        <TextArea
                            className="ant-input-textarea"
                            autosize={{ minRows: 2, maxRows: 6 }} 
                            disabled={true}
                            value={knowledge.checkout} />
                    </Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="改善行动(Act)" style={styles.span}>改善行动(Act)</span>
                        <TextArea
                            className="ant-input-textarea"
                            autosize={{ minRows: 2, maxRows: 6 }} 
                            disabled={true}
                            value={knowledge.improvementAction} />
                    </Col>
                </Row>
                <Row style={{ textAlign: 'left', visibility: this.props.visibilityLabel }} >
                    {
                        attachKnowledge && attachKnowledge.map((item, index) => {
                            if(item){
                                return(
                                    <div key={index}>
                                        <Header style={{ background: '#fff', padding: 0, height: '45px' }}>
                                            <Col span={8} >
                                                <span style={{ fontSize: '14px', fontWeight: 'bold', color: "#333" }}>{item.title}</span>
                                            </Col>
                                        </Header>
                                        {
                                            (() => {
                                                switch (item.knowType) {  
                                                    case "PSTC":
                                                        return <PreviewPSTC knowledge={item} planContent={item.planContent} />;

                                                    default:
                                                        return <PreviewGeneral knowledge={item} processs={item.processs} />;
                                                }
                                            })()
                                        }
                                    </div>
                                )
                            }
                        })
                    }
                </Row>
            </div>
        );
    }
}

export default PDCA;