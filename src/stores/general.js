import { observable, action } from "mobx";

import API from "../utils/API.js";

class GeneralStores {

    @observable process;        //流程列表
    @observable proType;        //流程分类类型
    @observable typeNum;        //流程分类序号
    @observable cateNum;        //风险评估序号
    @observable selectTypeNum;  //选中的流程分类


    constructor() {
        this.process = [];
        this.proType = "";
        this.selectTypeNum = null;
        this.typeNum = 0;
        // this.cateNum = 0;
    }

    //添加流程分类
    @action addProType = (value) => {
        this.process.push({
            typeNum: ++this.typeNum,
            proType: value,
            description: value,
            processCategoryContent: []
        });
    }

    //修改流程分类
    @action editProType = (key, value) => {
        const process = this.process;
        process.map((item) => {
            if (item.typeNum == key) {
                item.proType = value;
            }
        });

        //修改的内容和选中的流程分类相同的时候
        if (this.selectTypeNum == key) {
            this.proType = value;
        }
    }

    //删除流程分类
    @action deleteProType = (typeNum) => {
        var process = this.process;
        this.process = process.filter(item => item.typeNum != typeNum);
        //选中的流程分类设为null
        this.selectTypeNum = null;
        this.proType = "";
    }

    //流程分类拖拽排序
    onDragEnter = (info) => {
        // console.log(info);
    }
    onDrop = (info) => {
        // console.log(info);
        var process = this.process;

        //拖动的节点的key
        var dragNodesKeys = info.dragNodesKeys;          
        //拖动的节点的位置
        var dragPos;                                        
        process.map((item, index) => {
            if (item.typeNum == dragNodesKeys[0]) {
                dragPos = index;
            }
        });
        //落点的位置
        var dropPos; 
        if(info.dropPosition > process.length - 1) {
            dropPos = process.length - 1;
        } else if(info.dropPosition < 0) {
            dropPos = 0;
        }else{
            dropPos = info.dropPosition;
        }

        //更换两个节点的序列号
        process[dragPos].typeNum = process[dropPos].typeNum;
        process[dropPos].typeNum = dragNodesKeys[0];
        //更换两个节点的位置
        process.splice(dragPos, 1, ...process.splice(dropPos, 1, process[dragPos]));
        
        if (this.selectTypeNum && this.selectTypeNum[0] == dragNodesKeys[0]) {
            //当拖拽当前选中的节点的时候
            this.selectTypeNum = [String(process[dropPos].typeNum)];
        } else if (this.selectTypeNum && this.selectTypeNum[0] == process[dropPos].typeNum) {
            //当拖拽落点的位置节点和选中的节点相同的时候
            this.selectTypeNum = dragNodesKeys;
        }

        this.process = process;
    }

    //流程分类选中事件
    @action onSelectType = (key, node) => {
        const title = key.length ? node.node.props.title : "";
        this.selectTypeNum = key;
        this.proType = title;
    }

    //添加流程分类具体内容
    @action addCategory = (typeNum, value) => {
        this.process.map((item) => {
            if (item.typeNum == typeNum) {
                item.processCategoryContent.push({
                    typeNum: item.typeNum,
                    processType: item.proType,
                    title: value,
                    attachLink: "",
                    fileList: []
                });
            }
        });
    }

    //修改流程分类具体内容
    @action editCategory = (typeNum, value, categoryKey) => {
        const process = this.process;
        process.map((item) => {
            if (item.typeNum == typeNum) {
                item.processCategoryContent[categoryKey - 1].title = value;
            }
        });
        this.process = process;
    }

    @action deleteCategory = (typeNum, categoryKey) => {
        const process = this.process;
        process.map((item) => {
            if (item.typeNum == typeNum) {
                item.processCategoryContent.splice(categoryKey - 1, 1);
            }
        });
        this.process = process;
    }

    @action uploadFile = (record, info) => {
        // console.log(info);
        const process = this.process;
        process.map((item) => {
            if (record.typeNum == item.typeNum) {
                let key = record.key - 1;
                let data = {
                    typeNum: item.typeNum,
                    processType: record.processType,
                    title: record.title,
                    attachLink: info.file.response && info.file.response.data.data,
                    fileList: info.fileList,
                }
                item.processCategoryContent[key] = data;
            }
        });
        this.process = process;
    }

    @action emptyData = () => {
        this.process = [];
        this.proType = "";
        this.selectTypeNum = null;
        this.typeNum = 0;
        this.cateNum = 0;
    }

}

const generalStores = new GeneralStores();
export default generalStores;