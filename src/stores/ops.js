import { observable, action } from "mobx";

class OpsStores {

    @observable process;        //流程列表
    @observable typeNum;        //流程分类序号
    @observable cateNum;        //风险评估序号
    @observable selectTypeNum;  //选中的流程分类
    @observable selectProType;


    constructor() {
        this.process = [];
        this.selectTypeNum = null;
        this.typeNum = 0;
        this.cateNum = 0;
    }

    //添加流程分类
    @action addProType = (value) => {
        this.process.push({
            typeNum: ++this.typeNum,
            proType: value,
            description: value,
            processCategoryContent: []
        });
    }

    //删除流程分类
    @action deleteProType = (typeNum) => {
        var process = this.process;
        this.process = process.filter(item => item.typeNum != typeNum);
        //选中的流程分类设为null
        this.selectTypeNum = null;
    }

    //流程分类选中事件
    @action onSelectType = (key, node) => {
        this.selectTypeNum = key;
    }

    //添加分享评估
    @action addCategory = (typeNum, value) => {
        this.process.map((item) => {
            if (item.typeNum == typeNum) {
                item.processCategoryContent.push({
                    num: ++this.cateNum,
                    key: this.cateNum,
                    typeNum: item.typeNum,
                    processType: item.proType,
                    title: value,
                    attachLink: ""
                });
            }
        });
    }

}

const opsStores = new OpsStores();
export default opsStores;