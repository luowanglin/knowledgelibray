//时间戳转换
export const getTime = (e) => {
    if (e) {
        var d = new Date(e);
        d = d.getFullYear() + "/" + (d.getMonth() + 1) + "/" + d.getDate() + " " +
            (d.getHours() < 10 ? "0" + d.getHours() : d.getHours()) + ":" +
            (d.getMinutes() < 10 ? "0" + d.getMinutes() : d.getMinutes()) + ":" +
            (d.getSeconds() < 10 ? "0" + d.getSeconds() : d.getSeconds());
        return d;
    } else {
        return '';
    }
}