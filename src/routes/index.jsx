import React, { Component } from 'react';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
// import { HashRouter as Router, Route, Switch } from "react-router-dom";

import Home from '../components/Home';
import Add from '../components/Add';
import AttachPSTC from '../components/AttachPSTC';
import EditAttachPSTC from '../components/edit/EditAttachPSTC';
import Error from '../components/Error';
import Edit from '../components/Edit';
import Preview from '../components/Preview';
import PreviewProcess from "../components/PreviewProcess";


/**
 * 主页面路由 
 * */
export default class MainRouter extends Component{

    render() {
        return(
            <Router>
                <Switch>
                    <Route path={window.global.project_name} component={Home} exact/>
                    <Route path={window.global.project_name+'/add'} component={Add} exact/>
                    <Route path={window.global.project_name+'/addattachpstc'} component={AttachPSTC} />
                    <Route path={window.global.project_name+'/edit'} component={Edit} exact/>
                    <Route path={window.global.project_name+'/editattachpstc'} component={EditAttachPSTC} />
                    <Route path={window.global.project_name+'/preview'} component={Preview} />
                    <Route path={window.global.project_name+'previewprocess'} component={PreviewProcess} />
                    <Route component={Error} />
                </Switch>
            </Router>
        )
    }
}