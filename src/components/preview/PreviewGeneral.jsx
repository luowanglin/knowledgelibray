import React, { Component } from 'react';

import { observer, inject } from "mobx-react";

import API from "../../utils/API.js";

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import { Row, Col, Table, Button, Tree, Icon, Upload, Modal, Input, Layout, Divider, Message, Menu } from 'antd';
const { Header, Content, Sider } = Layout;
const SubMenu = Menu.SubMenu;
const TreeNode = Tree.TreeNode;


/**
 *  通用组件
 */
@inject("store")
@observer
class General extends Component {
    constructor(props) {
        super(props);

        this.state = {
            //流程分类ID
            processId: null
        }
        this.store = this.props.store;
    }

    getDataSource = (data) => {
        if(data) {
            const dataSource = [];
            if (!this.store.processId) {
                // data.map((item) => {
                //     item.processCategoryContent.map((cateItem, index) => {
                //         let itemData = cateItem;
                //         itemData['key'] = index;
                //         dataSource.push(itemData);
                //     })
                // })
                return dataSource;
            }
            data.map((item) => {
                if (item.processId == this.store.processId) {
                    item.processCategoryContent.map((cateItem, index) => {
                        dataSource.push({
                            key: index + 1,
                            num: index + 1,
                            contentId: cateItem.contentId,
                            processId: cateItem.processId,
                            processType: cateItem.processType,
                            title: cateItem.title,
                            attachLink: cateItem.attachLink,
                        });
                    })
                }
            })
            return dataSource
        }
    }

    onSelectType = (e) => {
        const store = this.store;
        store.onSelectType(e);
    }

    download = (record) => {
        const downloadUrl = API.BASE_URL + '/api/download' + "?" + $.param({
            contentId: record.contentId,
            token: API.token
        });
        var fileName;
        fetch(downloadUrl)
            .then(response => {
                fileName = response.headers.get("Content-Disposition").split("=").pop();
                fileName = decodeURIComponent(fileName);
                return response.blob()
            })
            .then(blob => {
                var link = document.createElement('a');
                link.href = window.URL.createObjectURL(blob);
                link.download = fileName;
                link.click();
                window.URL.revokeObjectURL(link.href);
            });
    }

    render() {
        const store = this.store;
        const { processs } = this.props;

        const columns = [{
            title: '序号',
            dataIndex: 'num',
            width: "15%",
        }, {
            title: '名称',
            dataIndex: 'title',
            width: "50%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        },
        {
            title: '关联项',
            key: 'attachLink',
            width: "35%",
            render: (text, record) => {
                let fileName;
                if(record.attachLink) {
                    let relUrl = record.attachLink.split("/");
                    fileName = relUrl[relUrl.length - 1];
                }
                return record.attachLink ? (
                    <a href="javascript:" onClick={() => this.download(record)} style={{color: "#666"}}>{fileName}</a>
                ) : null
            }
        }];
        return (
            // <div style={{ marginTop: 20, }} >
            //     <Row style={{ textAlign: 'left', }} >
            //         <Col span={6} style={{ height: '' }} >
            //             <div style={{ paddingBottom: 10 }} >
            //                 <span>流程分类</span>
                            
            //             </div>
            //             <div style={{ paddingRight: '20px' }} >
            //                 <Tree
            //                     selectedKeys={store.processId}
            //                     onSelect={this.onSelectType}>
            //                     { 
            //                         processs.map((item) =>
            //                             <TreeNode key={item.processId} title={item.proType} />
            //                         )
            //                     }
            //                 </Tree>
                            
            //             </div>
            //         </Col>
            //         <Col span={18}>
            //             <div style={{ paddingBottom: 10 }} >
            //                 <span>{store.proType}</span>
            //             </div>
            //             <Table columns={columns}
            //                 dataSource={this.getDataSource(processs)}
            //                 style={{ marginTop: '10px' }}
            //                 pagination={false} />
            //         </Col>
            //     </Row>
            // </div>
            <Layout style={{ background: '#fff', height: '75%', top: '0px', bottom: '0px', paddingTop: '20px' }}>
                {/* <Sider className="process-tree" width={300} style={{ position: 'relative', background: '#f8f8f8', marginRight: '20px', marginBottom: '5px' }}>
                    <Header style={{ background: '#f8f8f8', height: '20px', padding: 0 }}>
                        <Row >
                            <Col span={8} style={{ textAlign: 'left', paddingLeft: '20px', fontWeight: 'bold'}}>流程分类</Col>
                        </Row>
                    </Header>
                    <Divider style={{ width: 'auto', marginLeft: '20px', marginRight: '20px' }} />
                    <Content style={{ textAlign: 'left', marginLeft: '10px' }}>
                        <Tree
                            selectedKeys={store.processId}
                            onSelect={this.onSelectType}>
                            {
                                processs.map((item) =>
                                    <TreeNode key={item.processId} title={item.proType} />
                                )
                            }
                        </Tree>
                    </Content>
                </Sider> */}

                <Content >
                    {
                        processs && processs.length ? (
                            <Menu
                                selectedKeys={store.processId.slice()}
                                onClick={this.onSelectType}
                                mode="horizontal"
                                style={{ marginBottom: "15px" }}
                            >
                                {
                                    processs.map((item) =>
                                        <Menu.Item key={JSON.stringify(item.processId)}>{item.proType}</Menu.Item>
                                    )
                                }
                            </Menu>
                        ) : null
                    }
                    <Row >
                        <Col span={24} style={{ textAlign: 'left' }}>
                            <span style={{ fontWeight: 'bold' }} title={store.proType}>{store.proType}</span>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Table 
                                size="small"
                                columns={columns}
                                dataSource={this.getDataSource(processs)}
                                style={{ marginTop: '10px' }}
                                pagination={false} />
                        </Col>
                    </Row>
                </Content>
            </Layout>
        );
    }
}
export default General;