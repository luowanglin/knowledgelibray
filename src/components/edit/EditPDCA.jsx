import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { observer, inject } from "mobx-react";

import { Layout, Row, Col, Input, Select, Tag, Icon } from 'antd';
const { Header, Content } = Layout;
const { TextArea } = Input;

const styles = {
    row:{
        marginTop:20,
        textAlign:'left',
    },
    col:{
        display:'flex'
    },
    span:{
        minWidth:85
    }
};

@inject("store")
@observer
class PDCA extends Component {
    constructor(props) {
        super(props);

        this.store = this.props.store.editStores;
    }

    componentDidMount() {
        // const homeStores = this.props.store.homeStores;

        //获取所有计划列表
        // homeStores.getUserId((userId) => {
        //     this.props.store.pdcaStores.getAllPlans(userId);
        // });
        const homeStores = this.props.store.homeStores;
        homeStores.getKnowledgesByType("PSTC");
    }

    handleClose = (removedTag) => {
        const store = this.store;
        store.removePstcPlans(removedTag.id);
    }

    render() {
        const store = this.store;
        const homeStores = this.props.store.homeStores;
        const attachId = store.attachId ? store.attachId.slice() : undefined;

        return(
                <div>
                    <Row style={styles.row}>
                        <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="计划(Plan)" style={styles.span} >计划(Paln)</span>
                            <TextArea
                            className="ant-input-textarea"
                                value={store.plan}
                                placeholder="计划(Paln)"
                                onChange={(e) => { store.changePlan(e.target.value) }} 
                                autosize={{ minRows: 2, maxRows: 6 }}/>
                        </Col>
                    </Row>
                    <Row style={styles.row}>
                        <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="执行(Do)" style={styles.span}>执行(Do)</span>
                            <TextArea 
                                className="ant-input-textarea"
                                value={store.doSomething}
                                placeholder="执行(Do)" 
                                onChange={(e) => store.changeDoSomething(e.target.value)}
                                autosize={{ minRows: 2, maxRows: 6 }}/>
                        </Col>
                    </Row>
                    <Row style={styles.row}>
                        <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="检查(Ckeck)" style={styles.span}>检查(Check)</span>
                            <TextArea 
                                className="ant-input-textarea"
                                value={store.checkout}
                                placeholder="检查(Check)" 
                                onChange={(e) => store.changeCheckout(e.target.value) } 
                                autosize={{ minRows: 2, maxRows: 6 }}/>
                        </Col>
                    </Row>
                    <Row style={styles.row}>
                        <Col style={styles.col} span={24}>
                            <span className="ant-input-title" title="改善行动(Act)" style={styles.span}>改善行动(Act)</span>
                            <TextArea 
                                className="ant-input-textarea"
                                value={store.improvementAction}
                                placeholder="改善行动(Act)" 
                                onChange={(e) => store.changeImprovementAction(e.target.value) } 
                                autosize={{ minRows: 2, maxRows: 6 }}/>
                        </Col>
                    </Row>
                    <Row style={styles.row}>
                        <Col style={styles.col} span={24} >
                            <span className="ant-input-title" title="绑定计划" style={styles.span}>绑定计划</span>
                            <Select
                                value={attachId}
                                mode="multiple"
                                placeholder="请选择计划"
                                style={{width: "100%"}}
                                onChange={(value) => store.bindAttach(value)}
                            >
                                {
                                    homeStores.knowledgeLists.map((item, index) => 
                                        <Select.Option key={index} value={String(item.knowledgeId)}>{item.title}</Select.Option>
                                    )
                                }
                            </Select>
                        </Col>
                    </Row>
                    <Row style={styles.row}>
                        <Col style={styles.col} span={24} >
                            <span className="ant-input-title" title="添加(绑定)PSTC" style={styles.span}>添加(绑定)PSTC</span>
                            {
                                store.pstcPlans.map((item) =>
                                    <Tag
                                        closable
                                        key={item.id}
                                        style={{ background: '#fff' }}
                                        afterClose={() => this.handleClose(item)}
                                    >
                                        {item.title}
                                    </Tag>
                                )
                            }
                            <Link to={{ pathname: "/kms/editattachpstc" }} >
                                {/* <Tag
                                    style={{ background: '#fff', borderStyle: 'dashed' }}
                                >
                                    <Icon type="plus" /> New Tag
                                    </Tag> */}
                                <span className="ant-icon add" style={{ verticalAlign: "middle" }}></span>
                            </Link>
                        </Col>
                    </Row>
                </div>
            );
        }
}

export default PDCA;