import React,{Component} from 'react';

import { observer, inject } from "mobx-react";

import { Layout, Divider, Row, Col, Input, TreeSelect } from 'antd';
const TreeNode = TreeSelect.TreeNode;
const { TextArea } = Input;

const styles = {
    row:{
        marginTop:20,
        textAlign:'left',
    },
    col:{
        display:'flex'
    },
    span:{
        minWidth:85
    }
};

@inject("store")
@observer
class SubDetail extends Component {
    constructor(props) {
        super(props);

        this.store = this.props.store.addStores;
    }

    componentDidMount = () => {
        // const homeStores = this.props.store.homeStores;
        // homeStores.getUserId((userId) => {
        //     this.props.store.pdcaStores.getAllPlans(userId);
        //     homeStores.loadTreeData(userId);
        // });
        // this.props.store.pdcaStores.getAllPlans();
        //清空数据
        // this.store.emptyData();
    }

    handleSelectType = (value, node) => {
        this.store.onSelectType(value, node);
    }

    render(){
        const store = this.store;
        const treeData = this.props.store.homeStores.treeData;
        // const typeCode = store.typeCode.slice();

        // const options = [];
        // const loop = data => data.map((item, index) => {
        //     options.push({ 
        //         value: String(item.typeCode),
        //         label: item.knowledgeType,
        //         rootType: item.rootType,
        //         children: []
        //     });
        //     if (item.children) {
        //         item.children.map((childItem) => {
        //             options[index].children.push({
        //                 value: String(childItem.typeCode),
        //                 label: childItem.knowledgeType,
        //                 rootType: item.rootType
        //             });
        //         })
        //     }
        // })
        // loop(treeData);

        const loop = data => data.map((item, index) => {
            if(item.children) {
                return(
                    <TreeNode key={item.typeCode} rootType={item.rootType} fatherCode={item.fatherCode} value={item.typeCode} title={item.knowledgeType} >
                        {loop(item.children)}
                    </TreeNode>
                )
            }
            return <TreeNode key={item.typeCode} rootType={item.rootType} fatherCode={item.fatherCode} value={item.typeCode} title={item.knowledgeType}></TreeNode>
        })
        return(
            <div>
                <Row style={styles.row}>
                    <Col style={styles.col} span={8}>
                        <span className="ant-input-title" title="知识标题" style={styles.span} >知识标题</span>
                        <Input 
                            value={store.title} 
                            placeholder="知识标题" 
                            style={{width: 300}} 
                            onChange={(e) => store.changeTitle(e.target.value) }/>
                    </Col>
                    <Col style={styles.col} span={8}>
                        <span className="ant-input-title" title="知识编码" style={styles.span} >知识编码</span>
                        <Input 
                            value={store.serialNumber}
                            placeholder="知识编码" 
                            style={{ width: 300 }} 
                            onChange={(e) => store.changeSerialNumber(e.target.value)}/>
                    </Col>
                    <Col span={8} style={{visibility:this.props.visibilityTypeSelect,display:'flex'}}>
                        <span className="ant-input-title" title="知识类型" style={styles.span} >知识类型</span>
                        {/* <Cascader
                            value={typeCode}
                            placeholder="请选择知识类型"
                            changeOnSelect
                            options={options}
                            onChange={this.onChange} /> */}
                        <TreeSelect 
                            value={store.typeCode}
                            showSearch
                            style={{width: "100%"}} 
                            dropdownStyle={{ maxHeight: 240, overflow: 'auto' }}
                            placeholder="请选择知识类型"
                            allowClear
                            treeDefaultExpandAll
                            onSelect={this.handleSelectType} >

                            {loop(treeData)}
                        </TreeSelect>
                    </Col>
                </Row>
                <Row style={{marginTop:'20px',textAlign:'left'}} >
                    <Col span={24} style={{display:'flex'}} >
                        <span className="ant-input-title" title="知识描述" style={styles.span} >知识描述</span>
                        <TextArea 
                            className="ant-input-textarea"
                            autosize={{ minRows: 2, maxRows: 6 }} 
                            value={store.description}
                            placeholder="知识描述" 
                            onChange={(e) => { store.changeDescription(e.target.value) }} />
                    </Col>
                </Row>
            </div>
        );
    }
}

export default SubDetail;