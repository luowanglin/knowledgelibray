import React, { Component } from 'react';

import { observer, inject } from "mobx-react";

import API from "../../utils/API.js";

//公用删除提示Modal
import DeleteModal from "../common/DeleteModal";

import { Row, Col, Table, Button, Tree, Icon, Upload, Modal, Input, Layout, Divider, Message, Popconfirm } from 'antd';
const { Header, Content, Sider } = Layout;
const TreeNode = Tree.TreeNode;


/**
 *  通用组件
 */
@inject("store")
@observer
class General extends Component {
    constructor(props) {
        super(props);
        this.state = {
            delVisible: false,
            visible: false,
            cateVisible: false,
            proType: "",
            processId: "",
            categoryName: "",
            categoryKey: ""
        }

        this.store = this.props.store.editStores;
    }

    getDataSource = (data) => {
        if (data) {
            const dataSource = [];
            if (!this.store.processId) {
                
                return dataSource;
            }
            data.map((item) => {
                if (item.processId == this.store.processId) {
                    item.processCategoryContent.map((cateItem, index) => {
                        let fileList = [];
                        if(cateItem.attachLink) {
                            // let attUrl = cateItem.attachLink.split("//");
                            let relUrl = cateItem.attachLink.split("/");
                            let fileName = relUrl[relUrl.length - 1];
                            fileList.push({
                                uid: index + 1,
                                name: fileName,
                                status: 'done',
                                // url: cateItem.attachLink+"?token="+API.token+""
                            });
                        }else{
                            fileList = cateItem.fileList ? cateItem.fileList : [];
                        }
                        dataSource.push({
                            key: index + 1,
                            num: index + 1,
                            processId: cateItem.processId,
                            processType: cateItem.processType,
                            title: cateItem.title,
                            attachLink: cateItem.attachLink,
                            fileList: fileList
                        });
                    })
                }
            })

            return dataSource
        }
    }

    onSelectType = (key, node) => {
        const store = this.store;
        store.onSelectType(key, node);
    }

    addProcesss = () => {
        this.setState({
            visible: true,
            proType: "",
            processId: ""
        });
    }

    cancelProtypeModal = () => {
        this.setState({
            visible: false
        });
    }

    editProType = (e) => {
        const key = e.node.props.eventKey;
        const title = e.node.props.title;
        this.setState({
            visible: true,
            proType: title,
            processId: key
        });
    }

    //流程分类
    onOkProType = () => {
        if (!this.state.proType) {
            Message.warning("流程分类不能为空！");
            return;
        }
        if (this.state.processId) {
            //修改
            this.store.editProType(this.state.processId, this.state.proType);
        } else {
            //添加
            this.store.addProType(this.state.proType);
        }
        this.setState({ visible: false });
    }

    //删除流程分类
    deleteProType = () => {
        const store = this.store;
        if (store.processId.length) {
            this.setState({ delVisible: true });
            return;
        }
        Message.warning("未选中流程分类！");
    }
    onOkDeleteProType = () => {
        const store = this.store;
        store.deleteProType(store.processId[0]);
        this.setState({delVisible: false});
    }

    //流程分类拖拽排序
    onDragEnter = (info) => {
        this.store.onDragEnter(info);
    }
    onDrop = (info) => {
        this.store.onDrop(info);
    }

    addCateModal = () => {
        this.setState({
            cateVisible: true,
            categoryName: "",
            categoryKey: ""
        });
    }

    editCateModal = (text, record) => {
        this.setState({
            cateVisible: true,
            categoryName: record.title,
            categoryKey: record.key
        });
    }

    cancelCateModal = () => {
        this.setState({
            cateVisible: false
        });
    }

    //流程分类具体内容
    onCategoryOk = () => {
        if(!this.state.categoryName) {
            Message.warning("名称不能为空！");
            return;
        }
        const store = this.store;
        if(!this.state.categoryKey) {
            //添加
            store.addCategory(store.processId[0], this.state.categoryName);
        }else{
            //修改
            store.editCategory(store.processId[0], this.state.categoryName, this.state.categoryKey);
        }
        this.setState({cateVisible: false});
    }

    deleteCategory = (key) => {
        const store = this.store;
        store.deleteCategory(store.processId[0], key);
    }

    uploadFile = (record, file) => {
        const store = this.store;
        store.uploadFile(record, file);
    }

    render() {
        const store = this.store;
        const modalProTypeTitle = this.state.processId ? "编辑流程分类" : "添加流程分类";
        const modalCategoryTitle = this.state.categoryKey ? "编辑" + store.proType + "流程" : "添加" + store.proType + "流程";

        const columns = [{
            title: '序号',
            dataIndex: 'num',
            width: "15%",
        }, {
            title: '名称',
            dataIndex: 'title',
            width: "50%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        },
        {
            title: '关联项',
            key: 'attachLink',
            width: "20%",
            render: (text, record) => {
                return <span>
                    <Upload
                        multiple={false}
                        onChange={(file) => this.uploadFile(record, file)}
                        action={API.BASE_URL + "/api/uploadFile?token="+API.token+""}
                        fileList = {record.fileList}
                        >
                        {record.fileList.length ? null : <a href="javascript:" ><Icon type="link" theme="outlined" /></a>}
                    </Upload >
                </span >
        }
        }, {
            title: '操作',
            key: 'action',
            render: (text, record) => (
                <span>
                    <a className="ant-icon edit" href="javascript:" onClick={() => this.editCateModal(text, record)}></a>
                    <Popconfirm title="确认要删除吗?" onConfirm={() => this.deleteCategory(record.key)}>
                        <a className="ant-icon del_table" href="javascript:;" ></a>
                    </Popconfirm>
                </span>
            ),
            width: "15%",
        }];

        return (
            <Layout style={{ background: '#fff', height: '76%', top: '0px', bottom: '0px', paddingTop: '20px' }}>
                <Sider className="tree-box" width={300} style={{ position: 'relative', background: '#f8f8f8', marginRight: '20px', marginBottom: '5px' }}>
                    <Header style={{ background: '#f8f8f8', height: '20px', padding: 0 }}>
                        <Row style={{ lineHeight: '46px' }}>
                            <Col span={8} style={{ textAlign: 'left', paddingLeft: '20px', fontWeight: 'bold'}}>流程分类</Col>
                            <Col span={16} style={{ textAlign: 'right', paddingRight: '20px' }}>
                                <a className="ant-icon add" href="javascript:;" onClick={this.addProcesss}></a>
                                <a className="ant-icon del" href="javascript:;" onClick={this.deleteProType}></a>
                            </Col>
                            <DeleteModal
                                visible={this.state.delVisible}
                                onOk={this.onOkDeleteProType}
                                onCancel={() => this.setState({ delVisible: false })} />
                        </Row>
                    </Header>
                    <Divider style={{ width: 'auto', margin: '0 20px' }} />
                    <Content style={{ textAlign: 'left', margin: '0 30px 0 10px' }}>
                        <Tree
                            selectedKeys={store.processId.slice()}
                            onSelect={this.onSelectType}
                            onRightClick={this.editProType}
                            // draggable
                            // onDragEnter={this.onDragEnter}
                            onDrop={this.onDrop}>
                            {
                                store.processs.map((item) =>
                                    <TreeNode key={String(item.processId)} title={item.proType} />
                                )
                            }
                        </Tree>
                        <Modal
                            title={modalProTypeTitle}
                            maskClosable={false}
                            visible={this.state.visible}
                            onCancel={this.cancelProtypeModal}
                            cancelText="取消"
                            okText="确定"
                            footer={<div><button type="button" className="ant-btn ant-btn-primary" onClick={this.onOkProType}><span>确 定</span></button><button type="button" className="ant-btn" onClick={this.cancelProtypeModal}><span>取 消</span></button></div>}>
                            <Input placeholder="流程分类" value={this.state.proType} onChange={(e) => this.setState({ proType: e.target.value })} />
                        </Modal>
                    </Content>
                </Sider>

                <Content >
                    <Modal
                        title={modalCategoryTitle}
                        maskClosable={false}
                        visible={this.state.cateVisible}
                        onCancel={this.cancelCateModal}
                        cancelText="取消"
                        okText="确定"
                        footer={<div><button type="button" className="ant-btn ant-btn-primary" onClick={this.onCategoryOk}><span>确 定</span></button><button type="button" className="ant-btn" onClick={this.cancelCateModal}><span>取 消</span></button></div>}>
                        <Input placeholder="名称" value={this.state.categoryName} onChange={(e) => this.setState({ categoryName: e.target.value })} />
                    </Modal>
                    <Row >
                        <Col className="protype-title" span={12} style={{ textAlign: 'left' }}>
                            <span style={{fontWeight: 'bold'}} title={store.proType}>{store.proType}</span>
                        </Col>
                        <Col span={12}>
                            <Button
                                onClick={this.addCateModal}
                                disabled={store.processId.length ? false : true}
                                type="primary"
                                style={{ float: 'right' }}>
                                添加
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Col span={24}>
                            <Table 
                                size="small"
                                columns={columns}
                                dataSource={this.getDataSource(store.processs)}
                                style={{ marginTop: '10px' }}
                                pagination={false} />
                        </Col>
                    </Row>
                </Content>
            </Layout>
        );
    }
}
export default General;