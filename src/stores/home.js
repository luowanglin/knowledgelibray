import { observable, action } from "mobx";

import { Message } from "antd";

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import API from "../utils/API.js";


class HomeStores {

    //主页tree组件部分
    @observable userId;             //用户id
    @observable treeData;           //知识树列表
    @observable selectKeys;         //选中的树节点
    @observable rootType;           //知识归属类型
    @observable expandedKeys;       //展开的树节点
    @observable fatherCode;         //树节点的父节点
    @observable modifi;             //节点是否可删除
    @observable checkedKeys;        //选中的树节点复选框
    @observable checkedNodes;


    @observable loading;            //加载状态
    @observable perviewDetailData;  //查看页面 详细信息数据

    //主页表格部分
    @observable dataSource;         //表格数据
    //过滤数据
    @observable pageNo;             //页码
    @observable pageSize;           //所展示的条数
    @observable knowType;           //知识类型
    @observable keyword;            //关键字
    @observable pagination;         //分页


    @observable knowledgeId;        //知识ID
    @observable knowledgeLists;     //知识列表


    constructor() {
        this.userId = "";
        this.loading = false;
        this.treeData = [];
        this.selectKeys = [];
        this.rootType = "";
        this.checkedKeys = [];
        this.checkedNodes = [];
        this.expandedKeys = [];
        this.fatherCode = null;
        this.modifi = false;
        this.perviewDetailData = [];

        this.dataSource = [];
        this.pageNo = 1;
        this.pageSize = 10;
        this.knowType = "";
        this.keyword = "";
        this.pagination =  {
            size: "default",
            // defaultCurrent: 1,
            defaultPageSize: 10,
            showQuickJumper: true,
            pageSize: this.pageSize,
            onChange: (page) => {
                this.pageNo = page;
                this.onSearchKnowledge();
            },
            showTotal: function (total) {
                return '共' + total + '条数据';
            }
        };

        this.knowledgeId = "";
        this.knowledgeLists = [];

    }

    //设置loading状态
    @action setLoading = (isBoolean) => {
        this.loading = isBoolean;
    }

    //获取用户id
    // @action getUserId = (callback) => {
    //     $.ajax({
    //         url: API.BASE_URL + "userId/",
    //         type: 'POST',
    //         data: {
    //             username: 'test',
    //             password: 'test',
    //             token: API.token
    //         },
    //         success: (response) => {
    //             if(response.data.userId) {
    //                 this.userId = response.data.userId
    //                 callback && callback(response.data.userId);
    //                 return;
    //             }
    //         },
    //         error: (error) => {

    //         }
    //     }).always(() => {

    //     });
    // }

    //获取知识树列表
    @action loadTreeData = () => {
        this.loading = true;
        $.ajax({
            type: 'GET',
            url: API.BASE_URL + "/api/types?token="+API.token+"",
            dataType: 'json',
            // xhrFields: {
            //     withCredentials: true
            // },
            // data: {
            //     userId: this.userId,
            // },
            success: (response) => {
                if(response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (response.data) {
                    this.treeData = response.data;
                }
            },
            error: () => {

            }
        }).always(() => {
            this.loading = false;
        });
    }

    //删除知识树列表
    @action deleteKnowledgeType = (childrenCode, callback) => {
        const checkedKeys = this.checkedKeys.slice();
        childrenCode.map((item) => {
            checkedKeys.push(item);
        });

        $.ajax({
            url: API.BASE_URL + '/api/deleteKnowledgeType?token='+API.token+'',
            type: 'POST',
            contentType: 'application/json',
            // xhrFields: {
            //     withCredentials: true
            // },
            data: JSON.stringify(checkedKeys),
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (!response.data.error) {
                    callback && callback();
                    //刷新列表数据
                    this.loadTreeData();
                    this.checkedKeys = [];
                    this.checkedNodes.map((item) => {
                        if(item.key == this.selectKeys[0]) {
                            this.selectKeys = [];
                            this.fatherCode = null;
                        }
                    });
                    //刷新表格数据
                    this.onSearchKnowledge();
                    Message.success("数据删除成功！");
                    return;
                }
                Message.error("删除数据失败！");
            },
            error: () => {
                Message.error("删除数据失败！");
            }
        });
    }

    //知识树onSelect事件
    @action onSelect = (key, node) => {
        // const fatherCode = !node.node.props.fatherCode ? key[0] : null;
        const fatherCode = key[0];
        this.selectKeys = key;
        this.rootType = node.node.props.rootType;
        this.fatherCode = fatherCode;
        this.knowType = this.selectKeys.length ? node.node.props.value : "";
        // this.modifi = node.node.props.modifi;
        //页码设为第一页
        this.pageNo = 1;
        this.onSearchKnowledge();
    }

    //知识树复选框触发事件
    @action onCheck = (checkedKeys, info) => {
        this.checkedKeys = checkedKeys.checked;
        this.checkedNodes = info.checkedNodes;
    }

    //展开/手机树节点的时候
    @action onExpand = (expandedKeys) => {
        this.expandedKeys = expandedKeys;
    }

    //关键字搜索事件
    @action onSearch = (value) => {
        this.keyword = value;
        //页码设为第一页
        this.pageNo = 1;
        this.onSearchKnowledge();
    }

    //模糊查询
    @action onSearchKnowledge = () => {
        this.loading = true;
        // this.dataSource = [];
        $.ajax({
            type: 'GET',
            url: API.BASE_URL + "/api/findKnowledgeByKeyword?token="+API.token+"",
            dataType: 'json',
            // xhrFields: {
            //     withCredentials: true
            // },
            data: {
                // userId: this.userId,
                indexPage: this.pageNo,
                pageSize: this.pageSize,
                // knowType: this.knowType,
                typeCode: this.selectKeys[0],
                keyword: this.keyword,
            },
            success: (response) => {
                // if (response.code && response.code == "401") {
                //     alert(response.msg);
                //     window.location.href = "http://server.ylservice365.com:9090/cas-server/login";
                //     return;
                // }
                if (response.data) {
                    let data = [];
                    response.data.map((item, index) => {
                        data.push({
                            key: item.knowledgeId,
                            knowledgeId: item.knowledgeId,
                            userId: item.userId,
                            creatName: item.creatName,
                            title: item.title,
                            knowType: item.knowType,
                            typeCode: item.typeCode,
                            serialNumber: item.serialNumber,
                            description: item.description,
                            plan: item.plan,
                            doSomething: item.doSomething,
                            checkout: item.checkout,
                            improvementAction: item.improvementAction,
                            attachId: item.attachId,
                            createTime: item.time
                        });
                    });
                    //分页管理
                    this.pagination.current = this.pageNo;
                    this.pagination.total = response.totalElements;

                    this.dataSource = data;
                }
            },
            error: (error) => {
                Message.error("数据获取失败！");
            }
        }).always(() => {
            this.loading = false
        });
    }

    //知识类型过滤知识内容列表
    @action getKnowledgesByType = (knowType) => {
        $.ajax({
            type: 'GET',
            url: API.BASE_URL + '/api/findAllKnowledgesByKnowType?token=' + API.token + '',
            // xhrFields: {
            //     withCredentials: true
            // },
            data: {
                knowType: knowType
            },
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (response.data) {
                    this.knowledgeLists = response.data;

                    return;
                }
            },
            error: () => {

            }

        });
    }


    //删除知识报表数据
    @action deleteKnowledge = (knowledgeId) => {
        $.ajax({
            type: 'POST',
            url: API.BASE_URL + '/api/deleteKnowledge?token='+API.token+'',
            // xhrFields: {
            //     withCredentials: true
            // },
            data: {
                _method: 'DELETE',
                knowledgeId: knowledgeId
            },
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (response.data) {
                    this.pageNo = 1;
                    this.onSearchKnowledge();
                    return;
                }
                Message.error("数据删除失败！");
            },
            error: () => {
                Message.error("数据删除失败！");
            }
        });
    }

    @action onChangeKnowledgeId = (record) => {
        this.knowledgeId = record.knowledgeId;
    }

    //查询知识归属类型
    @action findKnowledgeType = (typeCode, callback) => {
        $.ajax({
            type: 'GET',
            url: API.BASE_URL + '/api/findKnowledgeType?token=' + API.token + '',
            data: {
                typeCode: typeCode
            },
            success: (response) => {
                if(response.data) {
                    callback && callback(response.data);
                    return;
                }
                Message.error("数据获取失败！");
            },
            error: (error) => {
                Message.error("数据获取失败！");
            }
        });
    }

    //获取详细知识条目
    // @action getKnowledgeContent = (knowledgeId, callback) => {
    //     this.loading = true;
    //     $.ajax({
    //         type: 'GET',
    //         url: API.BASE_URL + 'getKnowledgeContent?token='+API.token+'',
    //         data: {
    //             knowledgeId: knowledgeId,
    //         },
    //         success: (response) => {
    //             if (response.code && response.code == "401") {
    //                 alert(response.msg);
    //                 window.location.href = "http://server.ylservice365.com:9090/cas-server/login";
    //                 return;
    //             }
    //             if(response.data) {
    //                 callback && callback();
    //                 this.perviewDetailData = response.data;
    //                 return;
    //             }
    //             Message.error("获取数据失败！");
    //         },
    //         error: () => {
    //             Message.error("获取数据失败！");
    //         }
    //     }).always(() => {
    //         this.loading = false;
    //     });
    // }

    //清空数据
    @action emptyData = () => {
        this.userId = "";
        this.loading = false;
        this.treeData = [];
        this.selectKeys = [];
        this.rootType = "";
        this.checkedKeys = [];
        this.checkedNodes = [];
        this.fatherCode = null;
        this.modifi = false;
        this.perviewDetailData = [];

        // this.dataSource = [];
        this.pageNo = 1;
        this.pageSize = 10;
        this.knowType = "";
        this.keyword = "";
    }
    
}

const homeStores = new HomeStores();

homeStores.loadTreeData();
homeStores.onSearchKnowledge();

export default homeStores;