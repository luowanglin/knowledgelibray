//获取location query
import { getQueryString } from "../utils/getQueryString";

const API = {
    BASE_URL: window.global.context,
    token: getQueryString("token")
}

export default API;