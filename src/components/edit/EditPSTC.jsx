import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { observer, inject } from "mobx-react";

import moment from "moment";

import ProcessModal from "../ProcessModal.jsx";

//修改DatePicker语言配置
import locale from 'antd/lib/date-picker/locale/zh_CN';
import { Divider, Row, Col, Table, Input, DatePicker, Popconfirm, Button } from 'antd';
const { TextArea } = Input;

const styles = {
    row: {
        marginTop: 20,
        textAlign: 'left',
    },
    col: {
        display: 'flex'
    },
    span: {
        minWidth: 85
    }
};

@inject("store")
@observer
class EditPSTC extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isEditPage: true,
            modalData: {}
        }
        this.store = this.props.store.editStores;
    }

    componentDidMount() {

    }

    getDataSource = (data) => {
        const dataSource = [];
        //遍历执行进程列表
        data.map((item, index) => {
            dataSource.push({
                key: item.seqNum,
                id: item.id,
                knowledgeId: item.knowledgeId,
                proces: item.proces,
                pAdmin: item.pAdmin,
                tFinishTime: item.tFinishTime,
                completeStand: item.completeStand,
                render: item.render,
                result: item.result,
                opsSchedule: item.opsSchedule,
                planCode: item.planCode,
                seqNum: item.seqNum
            });
        })
        return dataSource;
    }

    changePlanAdmin = (e) => {
        const store = this.store;
        store.changePlanAdmin(e.target.value);
    }

    changeResultSum = (e) => {
        const store = this.store;
        store.changeResultSum(e.target.value);
    }

    addPlanContent = () => {
        this.setState({
            visible: true,
            modalData: {}
        });
    }

    editPlanContent = (e) => {
        this.setState({
            visible: true,
            modalData: e
        });
    }

    deletePlanContent = (key) => {
        this.store.deletePlanContent(key);
    }

    handleCancel = () => {
        this.setState({visible: false});
    }

    previewProces = (knowledgeId) => {
        const previewprocessStores = this.props.store.previewprocessStores;
        this.props.store.homeStores.onChangeKnowledgeId({ knowledgeId: knowledgeId });

        //获取知识详细信息
        previewprocessStores.getKnowledgeContent(knowledgeId);
    }

    render() {
        const store = this.store;

        const columns = [{
            title: '执行步骤',
            dataIndex: 'opsSchedule',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: 'P-负责人员',
            dataIndex: 'pAdmin',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: 'T-完成期限',
            dataIndex: 'tFinishTime',
            width: "15%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: 'S-责任人完成标准、主管人验收标准',
            dataIndex: 'completeStand',
            width: "20%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '向谁汇报',
            dataIndex: 'render',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '成效验收',
            dataIndex: 'result',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '运行维护流程',
            dataIndex: 'proces',
            width: "15%",
            render: (text, record) => {
                let proces = record.proces.split("/");
                return proces.map((item, index) => {
                    let knowledgeId = item.split(",")[0];
                    let title = item.split(",")[1];
                    return <Link 
                        to={{ pathname: "/kms/previewprocess" }}
                        key={knowledgeId} 
                        title={title} 
                        onClick={() => { this.previewProces(knowledgeId) }}
                        style={{ display: 'block', color: 'rgb(102, 102, 102)' }}>{title}</Link>
                })
            }
        }, {
            title: '操作',
            key: 'action',
            width: '10',
            render: (text, record) => (
                <span>
                    {/* <a className="ant-icon add" href="javascript:" onClick={() => this.handleEdit(record)}></a> */}
                    <a className="ant-icon edit" href="javascript:" onClick={() => this.editPlanContent(record)}></a>
                    <Popconfirm title="确认要删除吗?" onConfirm={() => this.deletePlanContent(record.key)}>
                        <a className="ant-icon del_table" href="javascript:;" ></a>
                    </Popconfirm>
                </span>
            ),
        }];

        return (
            <div>
                <Row style={styles.row}>
                    <Col style={styles.col} span={8}>
                        <span className="ant-input-title" title="计划负责人" style={styles.span} >计划总负责人</span>
                        <Input
                            placeholder="计划负责人"
                            value={store.planAdmin}
                            onChange={this.changePlanAdmin}
                            style={{ width: 300 }} />
                    </Col>
                    <Col style={styles.col} span={8}>
                        <span className="ant-input-title" title="最后完成期限" style={styles.span}>最后完成期限</span>
                        <DatePicker 
                            locale={locale}
                            showTime
                            format="YYYY-MM-DD HH:mm:ss"
                            placeholder="最后完成期限"
                            value={store.lastTime ? moment(store.lastTime) : null}
                            onChange={store.bindLastTime}
                            style={{ width: 300 }}/>
                    </Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="验收结案目标" style={styles.span}>验收结案目标</span>
                        <TextArea
                            className="ant-input-textarea"
                            placeholder="验收结案目标"
                            value={store.resultSum}
                            onChange={this.changeResultSum}
                            autosize={{ minRows: 2, maxRows: 6 }} />
                    </Col>
                </Row>
                {/* <Row style={styles.row} >
                    <Col style={styles.col} span={24}>
                        <span style={styles.span}>执行进程</span>
                        <Button onClick={this.addPlanContent}>添加进程</Button>
                    </Col>
                </Row> */}
                <Row style={{ marginTop: "20px" }}>
                    <Col span={8} style={{ textAlign: 'left', paddingTop: "10px" }}>
                        <span style={{ fontWeight: 'bold', }}>执行进程</span>
                    </Col>
                    <Col span={16}>
                        <Button type="primary" onClick={this.addPlanContent} style={{ float: "right" }}>添加进程</Button>
                    </Col>
                </Row>
                <Row style={styles.row} >
                    <Col style={styles.col} span={24}>
                        <div style={{ width: '100%' }} >
                            

                            <Table size="small" columns={columns} dataSource={this.getDataSource(store.planContent)} pagination={false} />

                            <ProcessModal
                                visible={this.state.visible}
                                isEditPage={this.state.isEditPage}
                                modalData={this.state.modalData}
                                handleCancel={this.handleCancel} />
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default EditPSTC;