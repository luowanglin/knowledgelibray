import homeStores from "./home";

import addStores from "./add";

import attachpstcStores from "./attachpstc";

import pdcaStores from "./pdca";

import opsStores from "./ops";

import pstcStores from "./pstc";

import generalStores from "./general";

import previewStores from "./preview";

import previewprocessStores from "./previewprocess";

import editStores from "./edit";

export { homeStores, addStores, pdcaStores, opsStores, pstcStores, generalStores, previewStores, previewprocessStores, editStores, attachpstcStores };