import { observable, action } from "mobx";

import { Message } from "antd";


import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import API from "../utils/API.js";

//时间戳转换
import { getTime } from "../utils/getTime";


class EditStores {

    @observable loading;            //加载状态
    @observable knowledgeId;
    @observable title;              //知识标题
    @observable serialNumber;       //知识编码
    @observable description;        //知识描述
    @observable rootType;           //知识归属类型
    @observable knowType;           //知识类型
    @observable typeFatherCode;     //父知识类型code
    @observable typeCode;           //知识类型code
    @observable devices;            //设备列表
    @observable attachDevices;      //绑定的设备列表
    @observable time;               //创建时间

    //PSTC部分
    @observable planAdmin;          //计划负责人
    @observable lastTime;           //最后完成期限
    @observable resultSum;          //验收结案目标
    @observable planContent;        //执行进程列表
    @observable seqNum;             //进程列表序号

    //PDCA部分
    @observable plan;               //计划
    @observable doSomething;        //执行
    @observable checkout;               //检查
    @observable improvementAction;    //改善行动
    @observable attachId;           //绑定计划
    @observable pstcPlans;          //绑定PSTC列表

    //通用组件部分
    @observable processs;           //流程列表
    @observable processId;          //流程分类ID
    @observable proType;            //流程分类名称
    @observable typeNum;            
    @observable cateNum;            //风险评估序列号


    constructor() {
        this.loading = false;
        this.knowledgeId = "";
        this.title = "";
        this.serialNumber = "";
        this.description = "";
        this.rootType = "";
        this.knowType = "";
        this.typeFatherCode = "";
        this.typeCode = "";
        this.devices = [];
        this.attachDevices = [];
        this.time = "";

        //PSTC部分
        this.planAdmin = "";
        this.lastTime = null;
        this.resultSum = "";
        this.planContent = [];
        this.seqNum = 0;

        //PDCA部分
        this.plan = "";
        this.doSomething = "";
        this.checkout = "";
        this.improvementAction = "";
        this.attachId = [];
        this.pstcPlans = [];

        //通用组件部分
        this.processs = [];
        this.processId = [];
        this.proType = "";
        this.typeNum = 0;
        this.cateNum = 0;

    }

    //获取详细知识条目
    @action getKnowledgeContent = (knowledgeId, successCallback, errorCallback) => {
        this.loading = true;
        $.ajax({
            type: 'GET',
            url: API.BASE_URL + '/api/getKnowledgeContent?token='+API.token+'',
            // xhrFields: {
            //     withCredentials: true
            // },
            data: {
                knowledgeId: knowledgeId
            },
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (response.data.data === null) {
                    Message.error("内容已被移除！");
                    errorCallback && errorCallback();
                    return;
                }
                let knowledge = response.data;

                this.knowledgeId = knowledge.knowledgeId;
                this.title = knowledge.title;
                this.serialNumber = knowledge.serialNumber;
                this.description = knowledge.description;
                this.knowType = knowledge.knowType;
                this.typeFatherCode = knowledge.typeFatherCode;
                this.typeCode = knowledge.typeCode;
                this.devices = knowledge.devices;
                this.attachDevices = knowledge.devices;
                this.time = knowledge.time;

                //PSTC部分
                let planContent = knowledge.planContent;
                this.planAdmin = knowledge.planAdmin;
                this.lastTime = knowledge.lastTime;
                this.resultSum = knowledge.resultSum;
                // this.planContent = response.data.planContent;
                this.planContent = planContent;
                this.seqNum = planContent.length ? planContent[planContent.length - 1].seqNum : 0;

                //PDCA部分
                this.plan = knowledge.plan;
                this.doSomething = knowledge.doSomething;
                this.checkout = knowledge.checkout;
                this.improvementAction = knowledge.improvementAction;
                this.attachId = knowledge.attachId ? knowledge.attachId.split(",") : null;

                //通用组件部分
                let processs = knowledge.processContent;
                let cateNum = 0;
                processs.map((item) => {
                    item.processCategoryContent.map((cateItem) => {
                        cateNum = ++cateNum;
                    })
                });
                this.processs = processs;
                this.processId = processs.length ? [JSON.stringify(processs[0].processId)] : [];
                this.proType = processs.length ? processs[0].proType : "";
                this.cateNum = cateNum;

                successCallback && successCallback();
            },
            error: () => {
                Message.error("获取数据失败！");
            }
        }).always(() => {
            this.loading = false;
        });
    }

    //设置知识归属类型
    @action setRootType = (data) => {
        this.rootType = data;
    }

    @action changeTitle = (value) => {
        this.title = value;
    }

    @action changeSerialNum = (value) => {
        this.serialNumber = value;
    }

    @action changeDesc = (value) => {
        this.description = value;
    }

    //添加绑定设备
    @action addDevice = (astCode, deviceName) => {
        this.devices.push({ "deviceName": deviceName, "astCode": astCode, knowledgeId: this.knowledgeId});
    }

    //删除绑定设备
    @action deleteDevice = (value) => {
        var devices = this.devices;
        this.devices = devices.filter(item => item["astCode"] !== value);
    }

    //确定绑定设备
    @action onOkattachDevices = () => {
        var devices = this.devices;
        var attachDevices = [];
        devices.map((item) => {
            // attachDevices.push({ "deviceName": item["deviceName"], "astCode": item["astCode"]});
            attachDevices.push(item);
        })
        this.attachDevices = attachDevices;
    }

    //取消绑定设备
    @action onCancelAttachDevices = () => {
        this.devices = [];
        this.attachDevices.map((item) => {
            // this.devices.push({ "deviceName": item["deviceName"], "astCode": item["astCode"]});
            this.devices.push(item);
        })
    }

    //PSTC部分动作
    @action bindLastTime = (date) => {
        this.lastTime = date;
    }

    @action changePlanAdmin = (value) => {
        this.planAdmin = value;
    }

    @action changeResultSum = (value) => {
        this.resultSum = value;
    }

    @action addPlanContent = (data) => {
        this.planContent.push(data);
    }

    @action editPlanContent = (key, data) => {       
        const planContent = this.planContent;
        for (var i = 0; i < planContent.length; i++) {
            if (planContent[i].seqNum == key) {
                let id = planContent[i]['id']
                planContent[i] = data;
                //如果存在id
                planContent[i]['id'] = id;
            }
        }
        this.planContent = planContent;
    }

    @action deletePlanContent = (key) => {
        const planContent = this.planContent;
        this.planContent = planContent.filter(item => item.seqNum !== key);
    }


    //PDCA部分动作
    @action changePlan = (value) => {
        this.plan = value;
    }

    @action changeDoSomething = (value) => {
        this.doSomething = value;
    }

    @action changeCheckout = (value) => {
        this.checkout = value;
    }

    @action changeImprovementAction = (value) => {
        this.improvementAction = value;
    }

    @action bindAttach = (value) => {
        this.attachId = value;
    }

    //添加pstc计划列表
    @action addPstcPlans = (data) => {
        this.pstcPlans.push({
            id: data.knowledgeId,
            title: data.title
        });
    }

    //移除pstc计划列表
    @action removePstcPlans = (id) => {
        const data = this.pstcPlans.filter(item => item.id !== id);
        this.pstcPlans = data;
    }

    //清空pstc计划列表
    @action emptyPstcPlans = () => {
        this.pstcPlans = [];
    }


    //通用模块
    @action onSelectType = (key, node) => {
        this.processId = key;
        this.proType = key.length ? node.node.props.title : "";
    }

    //添加流程分类
    @action addProType = (value) => {
        const processs = this.processs;
        const processId = this.processId;
        const typeNum = ++this.typeNum;
        processs.push({
            processId: processId + typeNum,
            proType: value,
            description: value,
            isAdd: true,
            processCategoryContent: []
        });
    }

    //修改流程分类
    @action editProType = (key, value) => {
        const processs = this.processs;
        processs.map((item) => {
            if(item.processId == key) {
                item.proType = value;
            }
        });
        //修改的内容和选中的流程分类相同的时候
        if(this.processId == key) {
            this.proType = value;
        }
    }

    //删除流程分类
    @action deleteProType = (key) => {
        var processs = this.processs
        this.processs = processs.filter(item => item.processId != key);
        //选中的流程分类设为null
        this.processId = [];
        this.proType = "";
    }

    //流程分类拖拽排序
    onDragEnter = (info) => {
        // console.log(info);
    }
    onDrop = (info) => {
        // console.log(info);
        var process = this.processs;

        //拖动的节点的key
        var dragNodesKeys = info.dragNodesKeys;
        //拖动的节点的位置
        var dragPos;
        process.map((item, index) => {
            if (item.processId == dragNodesKeys[0]) {
                dragPos = index;
            }
        });
        //落点的位置
        var dropPos;
        if (info.dropPosition > process.length - 1) {
            dropPos = process.length - 1;
        } else if (info.dropPosition < 0) {
            dropPos = 0;
        } else {
            dropPos = info.dropPosition;
        }

        //更换两个节点的序列号
        process[dragPos].processId = process[dropPos].processId;
        process[dropPos].processId = dragNodesKeys[0];
        //更换两个节点的位置
        process.splice(dragPos, 1, ...process.splice(dropPos, 1, process[dragPos]));

        if (this.processId && this.processId[0] == dragNodesKeys[0]) {
            //当拖拽当前选中的节点的时候
            this.processId = [String(process[dropPos].processId)];
        } else if (this.processId && this.processId[0] == process[dropPos].processId) {
            //当拖拽落点的位置节点和选中的节点相同的时候
            this.processId = dragNodesKeys;
        }

        this.process = process;
    }

    //添加流程分类具体内容
    @action addCategory = (key, value) => {
        const processs = this.processs;
        processs.map((item, index) => {
            if (item.processId == key) {
                item.processCategoryContent.push({
                    num: ++this.cateNum,
                    // processId: !item.isAdd ? item.processId : undefined,
                    processId: item.processId,
                    processType: item.proType,
                    title: value,
                    attachLink: "",
                    fileList: []
                });
            }
        });
    }

    //修改流程分类具体内容
    @action editCategory = (processId, value, categoryKey) => {
        const processs = this.processs;
        processs.map((item) => {
            if (item.processId == processId) {
                item.processCategoryContent[categoryKey - 1].title = value;
            }
        });
        this.processs = processs;
    }

    //删除流程分类具体内容
    @action deleteCategory = (processId, categoryKey) => {
        const processs = this.processs;
        processs.map((item) => {
            if (item.processId == processId) {
                item.processCategoryContent.splice(categoryKey - 1, 1);
            }
        });
        this.processs = processs;
    }

    //文件上传
    @action uploadFile = (record, info) => {
        const processs = this.processs;
        processs.map((item) => {
            if (record.processId == item.processId) {
                let key = record.key - 1;
                let data = {
                    processId: item.processId,
                    processType: record.processType,
                    title: record.title,
                    attachLink: info.file.response && info.file.response.data.data,
                    fileList: info.fileList,
                }
                item.processCategoryContent[key] = data;
            }
        });
        this.processs = processs;
    }


    //清空数据
    @action emptyData = () => {
        this.title = "";
        this.serialNumber = "";
        this.description = "";
        this.devices = [];
        this.attachDevices = [];
        this.time = "";

        //PSTC部分
        this.planAdmin = "";
        this.lastTime = null;
        this.resultSum = "";
        this.planContent = [];
        this.seqNum = 0;

        //PDCA部分
        this.plan = "";
        this.doSomething = "";
        this.checkout = "";
        this.improvementAction = "";
        this.attachId = [];
        this.pstcPlans = [];

        //通用组件部分
        this.processs = [];
        this.processId = [];
        this.proType = "";
        this.typeNum = 0;
        this.cateNum = 0;
    }

}

const editStores = new EditStores();
export default editStores;