import React, {Component} from "react";

import { Modal } from "antd";


export default class DeleteModal extends Component{

    render() {
        const { visible, onCancel, onOk } = this.props;
        return(
            <Modal
                title="提示"
                maskClosable={false}
                visible={visible}
                onCancel={onCancel}
                cancelText="取消"
                okText="确定"
                footer={<div><button type="button" className="ant-btn ant-btn-primary" onClick={onOk}><span>确 定</span></button><button type="button" className="ant-btn" onClick={onCancel}><span>取 消</span></button></div>}>
                <p>确认要删除数据吗?</p>
            </Modal>
        )
    }
}