import React,{Component} from 'react';

import { observer, inject } from "mobx-react";

import { Row, Col, Table, Button, Tree, Icon, Upload, Modal, Input } from 'antd';
const TreeNode = Tree.TreeNode;


/**
 *  远行维护组件
 */
@inject("store")
@observer
class OPS extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            cateVisible: false,     //分享评估Modal
            proType: "",            //流程分类名称
            categoryName: ""
        }

        this.store = this.props.store.opsStores;
    }

    addProtypeModal = () => {
        this.setState({
            proType: "",
            visible: true
        });
    }

    cancelProtypeModal = () => {
        this.setState({
            visible: false
        });
    }

    addCateModal = () => {
        this.setState({
            categoryName: "",
            cateVisible: true
        });
    }

    cancelCateModal = () => {
        this.setState({
            cateVisible: false
        });
    }

    //添加流程分类
    addProType = () => {
        const store = this.store;
        store.addProType(this.state.proType);
        this.setState({visible: false});
    }
    
    //删除流程分类
    deleteProType = () => {
        const store = this.store;

        Modal.info({
            title: '提示',
            content: (
                <div>
                    <p>是否要删除当前选中的分类和分类下面的风险评估数据?</p>
                </div>
            ),
            onOk() { 
                store.deleteProType(store.selectTypeNum[0]);
            },
        });
    }

    //添加分享评估
    addCategory = () => {
        const store = this.store;
        store.addCategory(store.selectTypeNum, this.state.categoryName);
        this.setState({cateVisible: false});
    }

    getDataSource = (data) => {
        const store = this.store;
        const dataSource = [];
        if(!store.selectTypeNum) {
            return dataSource;
        }
        data.map((item) => {
            if (item.typeNum == store.selectTypeNum) {
                item.processCategoryContent.map((cateItem) => {
                    dataSource.push(cateItem);
                })
            }
        })
        return dataSource
    }
   
    render() {
        const store = this.store;

        const columns = [{
            title: '序号',
            dataIndex: 'num',
            width: "15%",
        }, {
            title: '名称',
            dataIndex: 'title',
            width: "60%",
        }, 
        {
            title: '关联项',
            key: 'action',
            width: "25%",
            render: (text, record) => (
                <span>
                    <Upload >
                        <a href="javascript:" onClick={() => this.attachAction}><Icon type="link" theme="outlined" /></a>
                    </Upload>
                </span>
            ),
        }];
        return(
            <div style={{marginTop:20,}} >
                <Row style={{textAlign:'left',}} >
                    <Col span={6} style={{height:''}} >
                        <div style={{paddingBottom:10}} >
                            <span>流程分类</span>

                            {/* <a className="ant-icon add" href="javascript:;" onClick={this.showAddModal}></a>
                            <a className="ant-icon del" href="javascript:;" onClick={this.showDeleteModal}></a> */}
                            <Button 
                                disabled={store.selectTypeNum ? false : true}
                                onClick={this.deleteProType}
                                shape="circle" 
                                type="danger" 
                                icon="close" 
                                value="small" 
                                style={{float:'right',marginRight:'20px',borderWidth:0}} />
                            <Button 
                                onClick={this.addProtypeModal}
                                shape="circle" 
                                type="primary" 
                                icon="plus" 
                                value="small" 
                                style={{float:'right',marginRight:'20px',borderWidth:0}} />
                        </div>
                        <div style={{paddingRight:'20px'}} >
                            <Tree
                                checkedKeys={store.selectTypeNum}
                                onSelect={store.onSelectType}>
                                {
                                    store.process.map((item, index) => 
                                        <TreeNode key={item.typeNum} title={item.proType}/>
                                    )
                                }
                            </Tree>
                            <Modal
                                title="编辑流程分类"
                                visible={this.state.visible}
                                onCancel={this.cancelProtypeModal}
                                onOk={this.addProType}>
                                <Input placeholder="流程分类" value={this.state.proType} onChange={(e) => this.setState({proType: e.target.value})}/>
                            </Modal>
                        </div>
                    </Col>
                    <Col span={18}>
                        <div style={{paddingBottom:10}} >
                            <span>风险评估</span>
                            <Button 
                                disabled={store.selectTypeNum ? false : true}
                                onClick={this.addCateModal}
                                type="primary" 
                                style={{float: 'right'}}>
                                添加
                            </Button>
                            <Modal
                                title="添加风险评估"
                                visible={this.state.cateVisible}
                                onCancel={this.cancelCateModal}
                                onOk={this.addCategory}>
                                <Input placeholder="名称" value={this.state.categoryName} onChange={(e) => this.setState({categoryName: e.target.value})}/>
                            </Modal>
                         </div>
                        <Table columns={columns}
                            dataSource={this.getDataSource(store.process)}
                             style={{marginTop:'10px'}}
                             pagination={false} />
                    </Col>
                </Row>
            </div>
        );
    }
}
export default OPS;