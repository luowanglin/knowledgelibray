import { observable, action } from "mobx";

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import API from "../utils/API.js";

class PdcaStores {

    @observable plan;                   //计划
    @observable doSomething;            //执行
    @observable checkout;               //检查
    @observable impromentAction;        //改善行动
    @observable attachId;               //绑定计划
    @observable pstcPlans;              //(添加)pstc计划列表
    @observable allPlans;               //所有计划列表

    constructor() {
        this.allPlans = [];
        this.attachId = [];
        this.pstcPlans = [];
    }

    @action changePlan = (value) => {
        this.plan = value;
    }

    @action changeDoSomething = (value) => {
        this.doSomething = value;
    }

    @action changeCheckout = (value) => {
        this.checkout = value;
    }

    @action changeImpromentAction = (value) => {
        this.impromentAction = value;
    }

    //绑定计划列表
    @action bindAttach = (value) => {
        this.attachId = value;
    }

    //添加pstc计划列表
    @action addPstcPlans = (data) => {
        this.pstcPlans.push({
            id: data.knowledgeId,
            title: data.title
        });
    }

    //移除pstc计划列表
    @action removePstcPlans = (id) => {
        const data = this.pstcPlans.filter(item => item.id !== id);
        this.pstcPlans = data;
    }

    //清空pstc计划列表
    @action emptyPstcPlans = () => {
        this.pstcPlans = [];
    }


    //获取所有计划列表
    @action getAllPlans = () => {
        $.ajax({
            type: 'GET',
            url: API.BASE_URL + '/api/findAllPlans?token='+API.token+'',
            // xhrFields: {
            //     withCredentials: true
            // },
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (response.data) {
                    this.allPlans = response.data;

                    return;
                }
            },
            error: () => {
                
            }
            
        });
    }

    //清空数据
    @action emptyData = () => {
        this.plan = "";                   //计划
        this.doSomething = "";            //执行
        this.checkout = "";               //检查
        this.impromentAction = "";        //改善行动
        this.attachId = [];               //绑定计划
        this.pstcPlans = [];              //pstc计划列表
    }

}

const pdcaStores = new PdcaStores();
export default pdcaStores;