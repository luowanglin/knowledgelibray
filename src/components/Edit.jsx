import React, { Component } from 'react';

import { observer, inject } from "mobx-react";

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import API from "../utils/API.js";

//获取location query
import { getQueryString } from "../utils/getQueryString";

//PSTC类型组件
import EditPSTC from "./edit/EditPSTC.jsx";

//PDCA类型组件
import EditPDCA from "./edit/EditPDCA.jsx";

//通用类型组件
import EditGeneral from "./edit/EditGeneral.jsx";

//获取时间工具
// import { getCurrentDate } from "../utils/getDate";
//时间戳转换
import { getTime } from "../utils/getTime";


import { Layout, Divider, Row, Col, Button, Input, Spin, Message, Modal, AutoComplete } from 'antd';
const { Header, Content } = Layout;
const { TextArea } = Input;
const InputGroup = Input.Group;
const Option = AutoComplete.Option;

const styles = {
    row: {
        marginTop: 20,
        textAlign: 'left',
    },
    col: {
        display: 'flex'
    },
    span: {
        minWidth: 85
    }
};


/** 
 * 编辑页面
*/
@inject("store")
@observer
class Edit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false,
            devicename: "",
            deviceKey: "",
            astCode: "",
            dataSource: []
        }
        this.store = this.props.store.editStores;
    }

    componentDidMount() {
        //查询数据
        const store = this.store;
        const homeStores = this.props.store.homeStores;
        // const knowledgeId = this.props.store.homeStores.knowledgeId;
        if (homeStores.knowledgeId) {
            //清空数据
            // store.emptyData();
            //查询数据
            // store.getKnowledgeContent(homeStores.knowledgeId, () => {
            //     //数据已被删除的时候
            //     this.props.history.push('/kms');
            //     // this.props.store.homeStores.onSearchKnowledge();
            //     homeStores.onSearchKnowledge();
            // });
            return;
        }
        this.props.history.push('/kms');
    }

    saveData = (paramData) => {
        if(!this.store.title) {
            Message.warning("知识标题不能为空！");
            return;
        }else if(!this.store.serialNumber) {
            Message.warning('知识编码不能为空！');
            return;
        }
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: API.BASE_URL + '/api/updataKnowledge?token=' + API.token + '',
            // xhrFields: {
            //     withCredentials: true
            // },
            data: JSON.stringify(paramData),
            success: (response) => {
                if (response.code && response.code == "401") {
                    alert(response.msg);
                    window.location.href = window.global.login;
                    return;
                }
                if (!response.data.error) {
                    Message.success("数据保存成功！");
                    this.props.history.push('/kms');
                    //刷新数据
                    this.props.store.homeStores.onSearchKnowledge();
                    return;
                }
                Message.error(response.data.error);
            },
            error: (error) => {
                Message.error("数据保存失败！");
            }
        });
    }

    handleSave = () => {
        let paramData = {
            "knowledge": {
                //公共部分
                "knowledgeId": this.store.knowledgeId,
                "title": this.store.title,
                "knowType": this.store.knowType,
                "typeFatherCode": this.store.typeFatherCode,
                "typeCode": this.store.typeCode,
                "serialNumber": this.store.serialNumber,
                "description": this.store.description,
                "devices": this.store.attachDevices,
                "time": this.store.time,
            },
        }
        if(this.store.rootType === "PDCA") {
            let attachId = "";
            let attachIdData = [];
            this.store.attachId && this.store.attachId.map((item) => {
                attachIdData.push(item);
            })

            this.store.pstcPlans.map((item) => {
                attachIdData.push(item.id);
            })
            attachIdData.map((item, index) => {
                if (index >= attachIdData.length - 1) {
                    attachId += "" + item + "";
                } else {
                    attachId += "" + item + ",";
                }
            });
            paramData.knowledge["plan"] = this.store.plan;
            paramData.knowledge["doSomething"] = this.store.doSomething;
            paramData.knowledge["checkout"] = this.store.checkout;
            paramData.knowledge["improvementAction"] = this.store.improvementAction;
            paramData.knowledge["attachId"] = attachId;
        }else if(this.store.rootType === "PSTC") {
            let planContent = this.store.planContent;
            planContent.map((item) => {
                item['pstcId'] = planContent[0].pstcId;
                item['knowledgeId'] = this.store.knowledgeId;
            });
            paramData.knowledge["planAdmin"] = this.store.planAdmin;
            paramData.knowledge["lastTime"] = this.store.lastTime;
            paramData.knowledge["resultSum"] = this.store.resultSum;
            paramData["plans"] = planContent;
        }else{
            let processs = this.store.processs;
            processs.map((item) => {
                item['knowledgeId'] = this.store.knowledgeId;
                item['processId'] = !item.isAdd ? item['processId'] : undefined;
                item.processCategoryContent.map((contentItem) => {
                    contentItem['processId'] = !item.isAdd ? contentItem['processId'] : undefined;
                })
                item['isAdd'] = undefined;
            });
            paramData["processs"] = processs;
        }

        this.saveData(paramData);
    }

    handleAttachDev = () => {
        this.setState({visible: true});
    }

    //设备搜索
    onSearch = (value) => {
        this.setState({
            devicename: "",
            deviceKey: value,
            astCode: ""
        });
        $.ajax({
            url: API.BASE_URL + '/api/findDeviceByKeyword?token=' + API.token + '',
            type: 'GET',
            data: {
                keyword: value
            },
            success: (response) => {
                if (response.data && response.data.status === 200) {
                    const dataSource = [];
                    response.data.resultList.map((item) => {
                        dataSource.push(<Option key={item.ast_code} value={item.ast_code}>{item.name}</Option>);
                    })
                    this.setState({
                        dataSource: dataSource
                    });
                    return;
                }
            },
            error: (error) => {
                Message.error("数据获取失败！");
            }
        });
    }

    onSelectDev = (value, options) => {
        this.setState({
            devicename: options.props.children,
            deviceKey: value,
            astCode: value
        });
    }

    //取消绑定设备
    onCancelAttachDevices = () => {
        const store = this.store;
        store.onCancelAttachDevices();
        this.setState({ visible: false });
    }

    //确定绑定设备
    onOkattachDevices = () => {
        const store = this.store;
        store.onOkattachDevices();
        this.setState({ visible: false });
    }

    //添加设备
    addDevice = () => {
        if (this.state.astCode) {
            const store = this.store;
            for (let i = 0; i < store.devices.length; i++) {
                if (store.devices[i].astCode === this.state.astCode) {
                    Message.warning("您已经关联此设备！");
                    return false;
                }
            }
            store.addDevice(this.state.astCode, this.state.devicename);
            this.setState({
                devicename: "",
                deviceKey: "",
                astCode: ""
            });
            return;
        }
        Message.warning("请选择关联设备！");
    }

    //删除设备
    deleteDevice = (event) => {
        const store = this.store;
        store.deleteDevice(event.currentTarget.getAttribute("data-astcode"));
    }

    render() {
        const store = this.store;

        return(
            // <Spin spinning={store.loading} >
                <Layout className="home-container">
                    
                    <Header className="header-title" style={{ background: '#fff', padding: "0 20px" }}>
                        <Row type="flex" style={{ textAlign: 'left' }}>
                            <Col span={8} >
                                <Divider type="vertical" style={{ width: 2, backgroundColor: '#00c1d1', height: '20px', marginBottom: '4px', marginRight: "12px", marginLeft: 0 }} />

                                <span style={{ type: 'flex', fontSize: '16px', fontWeight: 'bold', color: "#333", align: 'middle' }}>编辑知识</span>
                            </Col>
                            <Col span={16} style={{ textAlign: 'right' }}>
                                <Button type="primary" style={{ marginRight: '18px' }} onClick={this.handleAttachDev}>关联设备</Button>
                                <Button type="primary" style={{ marginRight: '18px' }} onClick={this.handleSave}>保存</Button>
                                <Button className="common-btn" onClick={() => {
                                    this.props.history.push('/kms');
                                }}>取消</Button>
                            </Col>

                            <Modal
                                title="关联设备"
                                maskClosable={false}
                                visible={this.state.visible}
                                onCancel={this.onCancelAttachDevices}
                                footer={<div><button type="button" className="ant-btn ant-btn-primary" onClick={this.onOkattachDevices}><span>确 定</span></button><button type="button" className="ant-btn" onClick={this.onCancelAttachDevices}><span>取 消</span></button></div>}>
                                <InputGroup compact>
                                    <AutoComplete
                                        value={this.state.deviceKey}
                                        onSearch={this.onSearch}
                                        onSelect={this.onSelectDev}
                                        placeholder="请输入关联设备"
                                        dataSource={this.state.dataSource}
                                        style={{ width: '80%' }} />
                                    <Button type="primary" style={{ width: "20%" }} onClick={this.addDevice}>添加</Button>
                                </InputGroup>
                                <ul className="device-list">
                                    {
                                        store.devices && store.devices.map((item, index) =>
                                            <li key={index} className="device-item">
                                                <span>{item.deviceName}</span>
                                                <a href="javascript:;" className="cir_del" data-astcode={item.astCode} onClick={this.deleteDevice}></a>
                                            </li>
                                        )
                                    }
                                </ul>
                            </Modal>

                        </Row>
                        <Divider style={{ width: 'auto', marginTop: 0 }} />
                    </Header>
                    <Content style={{ background: '#fff', height: '100%', top: '0px', bottom: '0px', padding: '0 18px 18px', overflow: "auto" }}>
                        <Row style={styles.row}>
                            <Col style={styles.col} span={8}>
                            <span className="ant-input-title" title="知识标题" style={styles.span} >知识标题</span>
                                <Input
                                    value={store.title}
                                    onChange={(e) => store.changeTitle(e.target.value)}
                                    placeholder="知识标题"
                                    style={{ width: 300 }} />
                            </Col>
                            <Col style={styles.col} span={8}>
                                <span className="ant-input-title" title="知识编码" style={styles.span} >知识编码</span>
                                <Input
                                    value={store.serialNumber}
                                    onChange={(e) => store.changeSerialNum(e.target.value)}
                                    placeholder="知识编码"
                                    style={{ width: 300 }} />
                            </Col>
                        </Row>
                        <Row style={{ marginTop: '20px', textAlign: 'left' }} >
                            <Col span={24} style={{ display: 'flex' }} >
                        <span className="ant-input-title" title="知识描述" style={styles.span} >知识描述</span>
                                <TextArea
                                    className="ant-input-textarea"
                                    autosize={{ minRows: 2, maxRows: 6 }} 
                                    value={store.description}
                                    onChange={(e) => store.changeDesc(e.target.value)}
                                    placeholder="知识描述" 
                                    autosize={{ minRows: 2, maxRows: 6 }}/>
                            </Col>
                        </Row>

                        {
                            (() => {
                                // if(!store.loading) {
                                    switch (store.rootType) {
                                        case "PDCA":
                                            return <EditPDCA />;

                                        case "PSTC":
                                            return <EditPSTC />;

                                        default:
                                            return <EditGeneral />;
                                    }
                                // }
                            })()
                        }
                    </Content>
                </Layout>
            // </Spin>
        );
    }
}

export default Edit;