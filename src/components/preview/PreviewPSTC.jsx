import React, { Component } from 'react';

import { Link } from "react-router-dom";

import { observer, inject } from "mobx-react";

import moment from "moment";

import { Divider, Row, Col, Table, Input, DatePicker } from 'antd';
const { TextArea } = Input;

const styles = {
    row: {
        marginTop: 20,
        textAlign: 'left',
    },
    col: {
        display: 'flex'
    },
    span: {
        minWidth: 85
    }
};

@inject("store")
@observer
class PreviewPSTC extends Component {

    constructor(props) {
        super(props);
       
        this.store = this.props.store.homeStores;
    }
   
    getDataSource = (data) => {
        if(data) {
            const dataSource = [];
            data.map((item, index) => {
                let itemData = item;
                itemData['key'] = index;
                dataSource.push(itemData);
            })
            return dataSource;
        }
    }

    previewProces = (knowledgeId) => {
        console.log(knowledgeId);
        const previewprocessStores = this.props.store.previewprocessStores;
        this.store.onChangeKnowledgeId({knowledgeId: knowledgeId});

        //获取知识详细信息
        previewprocessStores.getKnowledgeContent(knowledgeId);
    }

    render() {
        const { knowledge, planContent } = this.props;

        const columns = [{
            title: '执行步骤',
            dataIndex: 'opsSchedule',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: 'P-负责人员',
            dataIndex: 'pAdmin',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: 'T-完成期限',
            dataIndex: 'tFinishTime',
            width: "10%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: 'S-责任人完成标准、主管人验收标准',
            dataIndex: 'completeStand',
            width: "25%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '向谁汇报',
            dataIndex: 'render',
            width: "15%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '成效验收',
            dataIndex: 'result',
            width: "15%",
            render: (text, record) => {
                return <span title={text}>{text}</span>
            }
        }, {
            title: '运行维护流程',
            dataIndex: 'proces',
            width: "15%",
            render: (text, record) => {
                let proces = record.proces.split("/");
                return proces.map((item, index) => {
                    let knowledgeId = item.split(",")[0];
                    let title = item.split(",")[1];
                    return <Link
                        to={{ pathname: "/kms/previewprocess" }}
                        key={knowledgeId}
                        title={title}
                        onClick={() => { this.previewProces(knowledgeId)}}
                        style={{ display: 'block', color: 'rgb(102, 102, 102)' }}>{title}</Link>
                })
            }
        }];
        return (
            <div>
                <Row style={styles.row}>
                    <Col style={styles.col} span={8}>
                        <span className="ant-input-title" title="计划负责人" style={styles.span} >计划总负责人</span>
                        <Input
                            disabled={true}
                            value={knowledge.planAdmin}
                            style={{ width: 300 }} />
                    </Col>
                    <Col style={styles.col} span={8}>
                        <span className="ant-input-title" title="最后完成期限" style={styles.span}>最后完成期限</span>
                        <DatePicker 
                            disabled={true}
                            showTime
                            format="YYYY-MM-DD HH:mm:ss"
                            placeholder=""
                            value={knowledge.lastTime ? moment(knowledge.lastTime) : null}
                            style={{ width: 300 }}/>
                    </Col>
                </Row>
                <Row style={styles.row}>
                    <Col style={styles.col} span={24}>
                        <span className="ant-input-title" title="验收结案目标" style={styles.span}>验收结案目标</span>
                        <TextArea
                            className="ant-input-textarea"
                            autosize={{ minRows: 2, maxRows: 6 }} 
                            disabled={true}
                            value={knowledge.resultSum}
                            autosize={{ minRows: 2, maxRows: 6 }}/>
                    </Col>
                </Row>
                <Row style={styles.row} >
                    <Col style={styles.col} span={24}>
                        <span style={styles.span} style={{fontWeight: "bold"}}>执行进程</span>
                    </Col>
                </Row>
                <Row style={styles.row} >
                    <Col style={styles.col} span={24}>
                        <div style={{ width: '100%' }} >

                            <Table size="small" columns={columns} dataSource={this.getDataSource(planContent)} pagination={false} />
                        </div>
                    </Col>
                </Row>
            </div>
        );
    }
}
export default PreviewPSTC;