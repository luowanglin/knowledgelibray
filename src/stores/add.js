import { observable, action } from "mobx";

import { Message } from "antd";

class AddStores {

    @observable title;                  //知识标题
    @observable serialNumber;           //知识编码
    @observable knowType;               //知识类型
    @observable rootType;               //知识归属类型
    @observable typeCode;               //知识类型code
    @observable fatherCode;             //知识类型父类型code
    @observable description;            //描述
    @observable devices;                //设备列表
    @observable attachDevices;          //绑定设备列表


    //初始化状态
    constructor() {
        this.title = "";
        this.knowType = "";
        this.rootType = "";
        this.typeCode = undefined;
        this.fatherCode = undefined;
        this.serialNumber = "";
        this.description = "";
        this.devices = [];
        this.attachDevices = [];
    }

    @action changeTitle = (value) => {
        this.title = value;
    }

    @action onSelectType = (value, node) => {
        const props = node.props;
        // const nodeTarget = node[node.length - 1];
        // this.knowType = nodeTarget ? nodeTarget.label : "";
        // this.rootType = nodeTarget ? node[0].rootType : "";
        this.knowType = props.title;
        this.rootType = props.rootType;
        this.typeCode = value;
        this.fatherCode = props.fatherCode;
    }

    @action changeSerialNumber = (value) => {
        this.serialNumber = value;
    }

    @action changeDescription = (value) => {
        this.description = value;
    }

    //添加绑定设备
    @action addDevice = (astCode, deviceName) => {
        this.devices.push({ "deviceName": deviceName, "astCode": astCode});
    }

    //删除绑定设备
    @action deleteDevice = (value) => {
        var devices = this.devices;
        this.devices = devices.filter(item => item["astCode"] !== value);
    }

    //确定绑定设备
    @action onOkattachDevices = () => {
        var devices = this.devices;
        var attachDevices = [];
        devices.map((item) => {
            attachDevices.push(item);
        })
        this.attachDevices = attachDevices;
    }

    //取消绑定设备
    @action onCancelAttachDevices = () => {
        this.devices = [];
        this.attachDevices.map((item) => {
            this.devices.push(item);
        })
    }

    //清空数据
    @action emptyData = () => {
        this.title = "";
        this.knowType = "";
        this.rootType = "";
        this.typeCode = undefined;
        this.serialNumber = "";
        this.description = "";
        this.devices = [];
        this.attachDevices = [];
    }

    //清空绑定设备
    @action empryAttachDevices = () => {
        this.devices = [];
        this.attachDevices = [];
    }
}

const addStores = new AddStores();

export default addStores;