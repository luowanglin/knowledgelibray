import { observable, action } from "mobx";

import { Message } from "antd";

class Attachpstc {

    @observable title;                  //知识标题
    @observable serialNumber;           //知识编码
    @observable knowType;               //知识类型
    @observable rootType;               //知识归属类型
    @observable typeCode;               //知识类型code
    @observable description;            //描述
    @observable devices;                //设备列表
    @observable attachDevices;          //绑定设备列表


    //初始化状态
    constructor() {
        this.title = "";
        this.knowType = "";
        this.rootType = "";
        this.typeCode = [];
        this.serialNumber = "";
        this.description = "";
        this.devices = [];
        this.attachDevices = [];
    }

    @action changeTitle = (value) => {
        this.title = value;
    }

    @action onSelectType = (value, node) => {
        const nodeTarget = node[node.length - 1];
        this.knowType = nodeTarget ? nodeTarget.label : "";
        this.rootType = nodeTarget ? node[0].rootType : "";
        this.typeCode = value;
    }

    @action changeSerialNumber = (value) => {
        this.serialNumber = value;
    }

    @action changeDescription = (value) => {
        this.description = value;
    }

    //添加绑定设备
    @action addDevice = (value) => {
        this.devices.push({ "deviceName": value });
    }

    //删除绑定设备
    @action deleteDevice = (value) => {
        var devices = this.devices;
        this.devices = devices.filter(item => item["deviceName"] !== value);
    }

    //确定绑定设备
    @action onOkattachDevices = () => {
        var devices = this.devices;
        var attachDevices = [];
        devices.map((item) => {
            attachDevices.push({ "deviceName": item["deviceName"] });
        })
        this.attachDevices = attachDevices;
    }

    //取消绑定设备
    @action onCancelAttachDevices = () => {
        this.devices = [];
        this.attachDevices.map((item) => {
            this.devices.push({ "deviceName": item["deviceName"] });
        })
    }

    //清空数据
    @action emptyData = () => {
        this.title = "";
        this.knowType = "";
        this.rootType = "";
        this.typeCode = [];
        this.serialNumber = "";
        this.description = "";
        this.devices = [];
        this.attachDevices = [];
    }

    //清空绑定设备
    @action empryAttachDevices = () => {
        this.devices = [];
        this.attachDevices = [];
    }
}

const attachpstcStores = new Attachpstc();

export default attachpstcStores;