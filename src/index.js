import "babel-polyfill";
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "mobx-react";
import * as stores from "./stores/index.js";
import './index.css';
import App from './components/App';
import registerServiceWorker from './registerServiceWorker';


ReactDOM.render(
    <Provider store={stores}>
        <App />
    </Provider>, 
    document.getElementById('root'));
registerServiceWorker();
