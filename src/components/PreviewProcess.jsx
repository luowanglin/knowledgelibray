import React, { Component } from 'react';

import { observer, inject } from "mobx-react";

import API from "../utils/API.js";

//获取location query
import { getQueryString } from "../utils/getQueryString";

//通用类型组件
import PreviewGeneral from "./preview/PreviewGeneral.jsx";

import { Layout, Divider, Row, Col, Button, Input, Spin, Modal } from 'antd';
const { Header, Content } = Layout;
const { TextArea } = Input;

const styles = {
    row: {
        marginTop: 20,
        textAlign: 'left',
    },
    col: {
        display: 'flex'
    },
    span: {
        minWidth: 85
    }
};


/**
 *  查看详细信息页面
 */
@inject("store")
@observer
class Preview extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false
        }
        this.store = this.props.store.previewprocessStores;
    }

    componentDidMount() {
        const homeStores = this.props.store.homeStores;
        if (homeStores.knowledgeId) {
            //清空数据
            this.store.emptyData();
            
            return;
        }
        //页面刷新之后返回主页
        this.props.history.push('/kms');
    }

    handleAttachDev = () => {
        this.setState({
            visible: true
        });
    }

    onCancel = () => {
        this.setState({
            visible: false
        });
    }

    render() {
        const { previewDetailData } = this.store;
        const knowledge = previewDetailData;

        return (
            // <Spin spinning={loading}>
            <Layout className="home-container">
                <Header className="header-title" style={{ background: '#fff', padding: "0 20px" }}>
                    <Row type="flex" style={{ textAlign: 'left' }}>
                        <Col span={8} >
                            <Divider type="vertical" style={{ width: 2, backgroundColor: '#00c1d1', height: '20px', marginBottom: '4px', marginRight: "12px", marginLeft: 0 }} />

                            <span style={{ type: 'flex', fontSize: '16px', fontWeight: 'bold', color: "#333", align: 'middle' }}>{knowledge && knowledge.title}</span>
                        </Col>
                        <Col span={16} style={{ textAlign: 'right' }}>
                            <Button type="primary" style={{ marginRight: '18px' }} onClick={this.handleAttachDev}>关联设备</Button>
                            <Button className="common-btn" onClick={() => { this.props.history.goBack(); }}>返回</Button>
                        </Col>

                        <Modal
                            title="关联设备"
                            maskClosable={false}
                            visible={this.state.visible}
                            onCancel={this.onCancel}
                            footer={null}>

                            <ul className="device-list" style={{ marginTop: 0 }}>
                                {
                                    knowledge && knowledge.devices.length ? knowledge.devices.map((item, index) =>
                                        <li key={index} className="device-item">
                                            <span>{item.deviceName}</span>
                                        </li>
                                    ) : (<span>暂无绑定设备！</span>)
                                }
                            </ul>
                        </Modal>

                    </Row>
                    <Divider style={{ width: 'auto', marginTop: 0 }} />
                </Header>
                <Content style={{ background: '#fff', height: '100%', top: '0px', bottom: '0px', padding: '0 18px 18px', overflow: "auto" }}>
                    <Row style={styles.row}>
                        <Col style={styles.col} span={8}>
                            <span className="ant-input-title" title="知识标题" style={styles.span} >知识标题</span>
                            <Input
                                disabled={true}
                                value={knowledge ? knowledge.title : null}
                                style={{ width: 300 }} />
                        </Col>
                        <Col style={styles.col} span={8}>
                            <span className="ant-input-title" title="知识编码" style={styles.span} >知识编码</span>
                            <Input
                                disabled={true}
                                value={knowledge ? knowledge.serialNumber : null}
                                style={{ width: 300 }} />
                        </Col>
                    </Row>
                    <Row style={{ marginTop: '20px', textAlign: 'left' }} >
                        <Col span={24} style={{ display: 'flex' }} >
                            <span className="ant-input-title" title="知识描述" style={styles.span} >知识描述</span>
                            <TextArea
                                className="ant-input-textarea"
                                autosize={{ minRows: 2, maxRows: 6 }}
                                disabled={true}
                                value={knowledge ? knowledge.description : null} />
                        </Col>
                    </Row>
                    {
                        (() => {
                            if (knowledge) {
                                return <PreviewGeneral knowledge={knowledge} processs={knowledge.processContent} store={this.props.store.previewprocessStores}/>;
                            }
                        })()
                    }
                </Content>
            </Layout>
            // </Spin>
        );
    };
}

export default Preview;