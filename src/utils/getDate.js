//获取当天时间并格式化
export const getCurrentDate = () => {
    let now = new Date();
    let nowYear = now.getFullYear();        //年
    let nowMonth = now.getMonth() + 1;      //月
    let nowDate = now.getDate();            //日
    let nowHours = now.getHours();          //小时
    if (nowHours < 10) {
        nowHours = "0" + nowHours;
    }
    let nowMinutes = now.getMinutes();      //分钟
    if (nowMinutes < 10) {
        nowMinutes = "0" + nowMinutes;
    }
    let nowSeconds = now.getSeconds();      //秒
    if (nowSeconds < 10) {
        nowSeconds = "0" + nowSeconds;
    }

    return nowYear + "/" + nowMonth + "/" + nowDate + "  " + nowHours + ":" + nowMinutes + ":" + nowSeconds;
}