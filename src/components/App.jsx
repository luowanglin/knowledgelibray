import React, { Component } from 'react';
import '../styles/App.css';

import MainRouter from "../routes/index";

class App extends Component {
    render() {
        return (
            <div className="App">
                <MainRouter />
            </div>
        );
    }
}

export default App;
