import React, { Component } from "react";

import { observer, inject } from "mobx-react";

//时间戳转换
import { getTime } from "../utils/getTime";
import moment from "moment";

import ajax from 'jquery/src/ajax/xhr.js';
import $ from 'jquery/src/ajax';

import API from "../utils/API.js";

//修改DatePicker语言配置
import locale from 'antd/lib/date-picker/locale/zh_CN';
import { Modal, Input, Form, DatePicker, Select } from "antd";
const FormItem = Form.Item;
const Option = Select.Option;

@inject('store')
@observer
class ProcessModal extends Component{
    constructor(props) {
        super(props);
        
        this.state = {};
        this.store = this.props.store.pstcStores;
        this.editStores = this.props.store.editStores;
    }

    componentDidMount() {
        //获取所有pstc计划列表
        const homeStores = this.props.store.homeStores;
        homeStores.getKnowledgesByType("运行维护");
    }

    componentWillReceiveProps(nextProps) {
        if(nextProps.visible) {
            let proces = nextProps.modalData.proces ? nextProps.modalData.proces.split("/") : undefined;
            this.setState({
                key: nextProps.modalData.key,
                id: nextProps.modalData.id,
                proces: proces,
                opsSchedule: nextProps.modalData.opsSchedule,
                pAdmin: nextProps.modalData.pAdmin,
                tFinishTime: nextProps.modalData.tFinishTime,
                completeStand: nextProps.modalData.completeStand,
                render: nextProps.modalData.render,
                result: nextProps.modalData.result,
            });
        }
    }

    handleOk = () => {
        if(!this.props.isEditPage) {
            //添加页面的时候
            const store = this.store;
            this.props.form.validateFields((err, values) => {
                if (!err) {
                    let data = values;
                    let proces = "";
                    data['key'] = this.state.key ? this.state.key : ++store.processKey;
                    data['seqNum'] = data['key'];
                    data['tFinishTime'] = getTime(values.tFinishTime);
                    data.proces && data.proces.map((item, index) => {
                        if (index >= data.proces.length - 1) {
                            proces += "" + item + "";
                        }else{
                            proces += "" + item + "/";
                        }
                    })
                    data['proces'] = proces;

                    if (this.state.key) {
                        store.editProcess(this.state.key, data);
                    } else {
                        store.addProcess(data);
                    }
                    this.onCancel();
                }
            });
        } else {
            this.props.form.validateFields((err, values) => {
                if (!err) {
                    let data = values;
                    let proces = "";
                    data['key'] = this.state.key ? this.state.key : ++this.editStores.seqNum;
                    data['seqNum'] = data['key'];
                    data['tFinishTime'] = getTime(values.tFinishTime);
                    data.proces && data.proces.map((item, index) => {
                        if (index >= data.proces.length - 1) {
                            proces += item;
                        } else {
                            proces += item + "/";
                        }
                    })
                    data['proces'] = proces;

                    if (this.state.key) {
                        this.editStores.editPlanContent(this.state.key, data);
                    } else {
                        this.editStores.addPlanContent(data);
                    }
                    this.onCancel();
                }
            });
        }
    }

    onCancel = () => {
        this.props.handleCancel();

        //重置表单
        setTimeout(() => {
            this.props.form.resetFields();
        }, 0);
    }


    render() {
        const homeStores = this.props.store.homeStores;
        const { handleCancel, visible } = this.props;

        const { getFieldDecorator } = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: { span: 24 },
                sm: { span: 6 },
            },
            wrapperCol: {
                xs: { span: 26 },
                sm: { span: 18 },
            },
        };

        const modalTitle = this.state.key ? "编辑执行进程" : "添加执行进程";
        const tFinishTime = this.state.tFinishTime ? moment(this.state.tFinishTime) : null;

        return(
            <Modal 
                title={modalTitle}
                onCancel={this.onCancel}
                visible={visible}
                maskClosable={false}
                cancelText="取消"
                okText="确定"
                footer={<div><button type="button" className="ant-btn ant-btn-primary" onClick={this.handleOk}><span>确 定</span></button><button type="button" className="ant-btn" onClick={handleCancel}><span>取 消</span></button></div>}>

                <Form>
                    <FormItem
                        {...formItemLayout}
                        label="执行步骤">

                        {getFieldDecorator('opsSchedule', {
                            rules: [{
                                required: true, message: '执行步骤不能为空',
                            }],
                            initialValue: this.state.opsSchedule
                        })(
                            <Input placeholder="执行步骤" />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="P-负责人员">

                        {getFieldDecorator('pAdmin', {
                            rules: [{
                                required: true, message: '负责人员不能为空',
                            }],
                            initialValue: this.state.pAdmin

                        })(
                            <Input placeholder="P-负责人员" />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="T-完成期限">

                        {getFieldDecorator('tFinishTime', {
                            rules: [{
                                required: true, message: '完成期限不能为空',
                            }],
                            initialValue: tFinishTime

                        })(
                            <DatePicker 
                                locale={locale}
                                showTime
                                style={{width: "100%"}}
                                format="YYYY-MM-DD HH:mm:ss" />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="完成、验收标准">

                        {getFieldDecorator('completeStand', {
                            rules: [{
                                required: true, message: '完成、验收标准不能为空',
                            }],
                            initialValue: this.state.completeStand

                        })(
                            <Input placeholder="S-责任人完成标准、主管人验收标准" />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="向谁汇报">

                        {getFieldDecorator('render', {
                            rules: [{
                                required: true, message: '向谁汇报不能为空',
                            }],
                            initialValue: this.state.render

                        })(
                            <Input placeholder="向谁汇报" />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="成效验收">

                        {getFieldDecorator('result', {
                            rules: [{
                                required: true, message: '成效验收不能为空',
                            }],
                            initialValue: this.state.result

                        })(
                            <Input placeholder="成效验收" />
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="运行维护流程">

                        {getFieldDecorator('proces', {
                            initialValue: this.state.proces

                        })(
                            // <Input placeholder="运行维护流程" />
                            <Select
                                mode="multiple"
                                placeholder="请选择运行维护流程"
                                style={{ width: '100%' }}
                            >
                                {
                                    homeStores.knowledgeLists.map((item, index) =>
                                        <Option key={index} value={item.knowledgeId+","+item.title}>{item.title}</Option>
                                    )
                                }
                            </Select>
                        )}
                    </FormItem>
                </Form>

            </Modal>
        )
    }
}
const FormModal = Form.create()(ProcessModal);
export default FormModal;