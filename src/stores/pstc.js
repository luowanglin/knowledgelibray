import { observable, action } from "mobx";

class PSTCStores{

    @observable processKey;                 //添加进程key
    @observable processList = [];           //执行进程
    @observable planAdmin;                  //负责人
    @observable lastTime;                   //完成期限
    @observable resultSum;                  //结案目标

    constructor() {
        this.processKey = 0;
        this.processList = [];
        this.planAdmin = "";
        this.lastTime = null;
        this.resultSum = "";
    }

    @action changePlanAdmin = (value) => {
        this.planAdmin = value;
    }

    @action changeResultSum = (value) => {
        this.resultSum = value;
    }

    @action bindLastTime = (date, dateString) => {
        this.lastTime = date;
    }

    //添加进程
    @action addProcess = (data) => {
        this.processList.push(data);
    }

    //修改进程
    @action editProcess = (key, data) => {
        const processList = this.processList;
        for (var i = 0; i < processList.length; i++) {
            if (processList[i].key == key) {
                processList[i] = data;
            }
        }
        this.processList = processList;
    }

    //删除进程
    @action processDelete = (key) => {
        var processList = this.processList;
        this.processList = processList.filter(item => item.key !== key);
    }

    //清空数据
    @action emptyData = () => {
        this.processKey = 0;
        this.processList = [];
        this.planAdmin = "";
        this.lastTime = null;
        this.resultSum = "";
    }

}

const pstcStores = new PSTCStores();
export default pstcStores;